/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.klaylike;

import org.clavierdvorak.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 *
 * @author twilight
 */
public class KlayTyper implements Typer {

    public static class FingerInfo {
        public int finger;
        public float[] fingerPosition = new float[2];

        public FingerInfo(int finger, float x, float y) {
            this.finger = finger;
            this.fingerPosition[0] = x;
            this.fingerPosition[1] = y;
        }

    }
    
    public static Map<Key, FingerInfo> leftFingers = new HashMap<Key, FingerInfo>() {{
            put(Conversion.xkbToScancode.get("TLDE"), new FingerInfo(4, -11, +10));
            put(Conversion.xkbToScancode.get("AE01"), new FingerInfo(4, -5, +10));
            put(Conversion.xkbToScancode.get("AE02"), new FingerInfo(4, +1, +10));
            put(Conversion.xkbToScancode.get("AE03"), new FingerInfo(3, +1, +10));
            put(Conversion.xkbToScancode.get("AE04"), new FingerInfo(2, +1, +10));
            put(Conversion.xkbToScancode.get("AE05"), new FingerInfo(1, +1, +10));
            put(Conversion.xkbToScancode.get("AE06"), new FingerInfo(1, +7, +10));
            put(Conversion.xkbToScancode.get("AD01"), new FingerInfo(4, -2, +5));
            put(Conversion.xkbToScancode.get("AD02"), new FingerInfo(3, -2, +5));
            put(Conversion.xkbToScancode.get("AD03"), new FingerInfo(2, -2, +5));
            put(Conversion.xkbToScancode.get("AD04"), new FingerInfo(1, -2, +5));
            put(Conversion.xkbToScancode.get("AD05"), new FingerInfo(1, +4, +5));
            put(Conversion.xkbToScancode.get("AC01"), new FingerInfo(4, 0, 0));
            put(Conversion.xkbToScancode.get("AC02"), new FingerInfo(3, 0, 0));
            put(Conversion.xkbToScancode.get("AC03"), new FingerInfo(2, 0, 0));
            put(Conversion.xkbToScancode.get("AC04"), new FingerInfo(1, 0, 0));
            put(Conversion.xkbToScancode.get("AC05"), new FingerInfo(1, +6, 0));
            put(Conversion.xkbToScancode.get("LSGT"), new FingerInfo(4, -3, -5));
// Dactylo
            put(Conversion.xkbToScancode.get("AB01"), new FingerInfo(4, 3, -5));
            put(Conversion.xkbToScancode.get("AB02"), new FingerInfo(3, 3, -5));
            put(Conversion.xkbToScancode.get("AB03"), new FingerInfo(2, 3, -5));
            put(Conversion.xkbToScancode.get("AB04"), new FingerInfo(1, +3, -5));
  
// A_Shape : see setHandPlacement
/*
            put(Conversion.xkbToScancode.get("AB01"), new FingerInfo(3, -3, -5));
            put(Conversion.xkbToScancode.get("AB02"), new FingerInfo(2, -3, -5));
            put(Conversion.xkbToScancode.get("AB03"), new FingerInfo(1, -3, -5));
            put(Conversion.xkbToScancode.get("AB04"), new FingerInfo(1, +3, -5));
*/
            put(Conversion.xkbToScancode.get("AB05"), new FingerInfo(1, +9, -5));
            put(Conversion.xkbToScancode.get("SPCE"), new FingerInfo(0, 0, -5));
            put(Conversion.xkbToScancode.get("LFSH"), new FingerInfo(4, -9, -5));
        }
    };
    public static Map<Key, FingerInfo> rightFingers = new HashMap<Key, FingerInfo>() {
        {
            put(Conversion.xkbToScancode.get("AE07"), new FingerInfo(1, -5, +10));
            put(Conversion.xkbToScancode.get("AE08"), new FingerInfo(1, +1, +10));
            put(Conversion.xkbToScancode.get("AE09"), new FingerInfo(2, +1, +10));
            put(Conversion.xkbToScancode.get("AE10"), new FingerInfo(3, +1, +10));
            put(Conversion.xkbToScancode.get("AE11"), new FingerInfo(4, +1, +10));
            put(Conversion.xkbToScancode.get("AE12"), new FingerInfo(4, +7, +10));
            put(Conversion.xkbToScancode.get("AD06"), new FingerInfo(1, -8, +5));
            put(Conversion.xkbToScancode.get("AD07"), new FingerInfo(1, -2, +5));
            put(Conversion.xkbToScancode.get("AD08"), new FingerInfo(2, -2, +5));
            put(Conversion.xkbToScancode.get("AD09"), new FingerInfo(3, -2, +5));
            put(Conversion.xkbToScancode.get("AD10"), new FingerInfo(4, -2, +5));
            put(Conversion.xkbToScancode.get("AD11"), new FingerInfo(4, +4, +5));
            put(Conversion.xkbToScancode.get("AD12"), new FingerInfo(4, +10, +5));
            put(Conversion.xkbToScancode.get("AC06"), new FingerInfo(1, -6, 0));
            put(Conversion.xkbToScancode.get("AC07"), new FingerInfo(1, 0, 0));
            put(Conversion.xkbToScancode.get("AC08"), new FingerInfo(2, 0, 0));
            put(Conversion.xkbToScancode.get("AC09"), new FingerInfo(3, 0, 0));
            put(Conversion.xkbToScancode.get("AC10"), new FingerInfo(4, 0, 0));
            put(Conversion.xkbToScancode.get("AC11"), new FingerInfo(4, +6, 0));
            put(Conversion.xkbToScancode.get("BKSL"), new FingerInfo(4, +12, 0));
            put(Conversion.xkbToScancode.get("AB06"), new FingerInfo(1, -3, -5));
            put(Conversion.xkbToScancode.get("AB07"), new FingerInfo(1, +3, -5));
            put(Conversion.xkbToScancode.get("AB08"), new FingerInfo(2, +3, -5));
            put(Conversion.xkbToScancode.get("AB09"), new FingerInfo(3, +3, -5));
            put(Conversion.xkbToScancode.get("AB10"), new FingerInfo(4, +3, -5));
            put(Conversion.xkbToScancode.get("SPCE"), new FingerInfo(0, 0, -5));
            put(Conversion.xkbToScancode.get("RTRN"), new FingerInfo(4, 0, +18));
            put(Conversion.xkbToScancode.get("RTSH"), new FingerInfo(4, +5, +5));
            put(Conversion.xkbToScancode.get("RALT"), new FingerInfo(0, +10, -5));
    }};
    protected Set<Key> specialKeys = new HashSet<Key>() {{
        add(Conversion.xkbToScancode.get("LFSH"));
        add(Conversion.xkbToScancode.get("RTRN"));
        add(Conversion.xkbToScancode.get("RTSH"));
        add(Conversion.xkbToScancode.get("RALT"));
    }};
    // could use this fixed-order list to index some arrays in KlayStatistic instead of having all those maps (TODO performance)
    protected List<Key> allKeys = new ArrayList<Key>() {{
        Set<Key> allKeysSet = new CopyOnWriteArraySet<Key>();
        allKeysSet.addAll(leftFingers.keySet());
        allKeysSet.addAll(rightFingers.keySet());
        addAll(allKeysSet);
    }};
    protected KlayHand leftHand = new KlayHand();
    protected KlayHand rightHand = new KlayHand();

    protected Key previousKey = null;
    protected KlayHand previousHand = null;
    protected FingerInfo previousFinger = null;

    protected void add(float incr, Map<Key, Float> map, Key key) {
        map.put(key, map.get(key) + incr);
    }

    protected void add(int incr, Map<Key, Integer> map, Key key) {
        map.put(key, map.get(key) + incr);
    }

    protected void countOnInit(KlayStatistic stat) {
        stat.initMapsWithKeys(allKeys);
    }

    protected void countOnKey(KlayStatistic stat, Key key, KlayHand hand) {
        add(1, stat.pressedKeys, key);
        // Klay does not really handle which-hand-types-what like this I think (TODO Klay compatibility)
        if (hand != null) {
            FingerInfo f = null;
            if (hand == rightHand) {
                f = rightFingers.get(key);
                stat.rightFingerEnergies[f.finger] += hand.fingerEnergy;
                stat.rightPalmEnergy += hand.palmEnergy;
                add(hand.palmEnergy + hand.fingerEnergy, stat.energyPerKey, key);
                ++stat.rightFingerCounts[f.finger];
            } else if (hand == leftHand) {
                f = leftFingers.get(key);
                stat.leftFingerEnergies[f.finger] += hand.fingerEnergy;
                stat.leftPalmEnergy += hand.palmEnergy;
                add(hand.palmEnergy + hand.fingerEnergy, stat.energyPerKey, key);
                ++stat.leftFingerCounts[f.finger];
            }
            ++stat.totalFingerCount;
            // Should treat only non-special keys here (TODO klay compatibility)
            if (!specialKeys.contains(key)) {
                if (previousKey != null) {
                    add(1, stat.consecutivePressedKeys.get(previousKey), key);
                    if (previousFinger!=null) { 
                    	// Here klay was counting foreach key pair typed consecutivelly by a same finger.
                    	// It creates lazily enties in the map (of the form "AC02-AD02"), initialize it to 1 the first time it is encountered and incrementing it after.
                    	// This is a deduced statitistic that we can process at the end from the digram table and the fingers descriptions.
                    	// Should check we properly process self.stats['repeating_finger'] (TODO klay compatibility)
                    	if (previousHand != hand && f.finger != 0 && previousFinger.finger != 0) {
                    		stat.alternance++;
                    	}
                    	maintainStats(stat, key, f, hand);
                    }
                }
                previousKey = key;
                previousFinger = f;
                previousHand = hand;
            } else {
                stat.totalPressedSpecialKeys++;
            }
        } else {
            // hand==null
            // add 1 to alternance to be fairer and close to klay
            // do not! it is taken into account at statistic level
            //stat.alternance++;
            previousHand = null;
            previousFinger = null;
        }
    }
    
    protected void maintainStats(KlayStatistic stat, Key key, FingerInfo f, KlayHand hand) {
    	
    }
    
    protected void countOnEnd(KlayStatistic stat) {
        stat.processDerivedValues();
    }

    public Statistic typeKeySequence(Iterator<Key> keySequence) {
    	return typeKeySequence(keySequence, new KlayStatistic());
    }

    public Statistic typeKeySequence(Iterator<Key> keySequence, KlayStatistic stat) {
        countOnInit(stat);
        try {
            while (keySequence.hasNext()) {
                final Key key = keySequence.next();
                KlayHand hand = typeKey(stat, key);
                countOnKey(stat, key, hand);
            }
        } catch (RuntimeException e) {
            if (!"Key Sequence Ended".equals(e.getMessage())) {
                e.printStackTrace();
            }
        }
        countOnEnd(stat);
        return stat;
    }

    protected KlayHand typeKey(KlayStatistic stat, Key key) {
        boolean isLeft = Hand.isLeftHand(key);
        boolean isRight = Hand.isRightHand(key);
        if (isLeft) {
            if (isRight) {
                // space?
                leftHand.resetFingers();
                rightHand.resetFingers();
                return null;
            } else {
                FingerInfo fingerInfo = leftFingers.get(key);
                leftHand.moveFinger(fingerInfo.finger, fingerInfo.fingerPosition);
                rightHand.resetFingers();
                return leftHand;
            }
        } else {
            if (isRight) {
                FingerInfo fingerInfo = rightFingers.get(key);
                rightHand.moveFinger(fingerInfo.finger, fingerInfo.fingerPosition);
                leftHand.resetFingers();
                return rightHand;
            } else {
                System.out.println("Neither left nor right hand can type key '"+key+"'");
                return null;
            }
        }
    }

    public enum HandPlacement {
    	Dactylo,
    	A_Shape;
    	
    	public String toString() {
        	switch (this) {
        		case Dactylo: return "Dactylo"; 
        		case A_Shape: return "Placement A"; 
        	}
			return null;
    	}
    }
    
    protected HandPlacement currentHandPlacement = HandPlacement.Dactylo;
    
    public HandPlacement getHandPlacement() {
    	return currentHandPlacement;
    }
    
    public void setHandPlacement(HandPlacement placement) {
    	currentHandPlacement = placement;
    	switch (placement) {
    		case Dactylo: { 
    			leftFingers.put(Conversion.xkbToScancode.get("AB01"), new FingerInfo(4, 3, -5));
    			leftFingers.put(Conversion.xkbToScancode.get("AB02"), new FingerInfo(3, 3, -5));
    			leftFingers.put(Conversion.xkbToScancode.get("AB03"), new FingerInfo(2, 3, -5));
    			leftFingers.put(Conversion.xkbToScancode.get("AB04"), new FingerInfo(1, +3, -5));
    			break;
    		}
    		case A_Shape: {
    			leftFingers.put(Conversion.xkbToScancode.get("AB01"), new FingerInfo(3, -3, -5));
    			leftFingers.put(Conversion.xkbToScancode.get("AB02"), new FingerInfo(2, -3, -5));
    			leftFingers.put(Conversion.xkbToScancode.get("AB03"), new FingerInfo(1, -3, -5));
    			leftFingers.put(Conversion.xkbToScancode.get("AB04"), new FingerInfo(1, +3, -5));
    			break;
    		}
    	}
    }
    
}
