/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.klaylike;

/**
 *
 * @author twilight
 */
public class KlayHand {

    float palmStiffness = 3;
    float palmPosition[] = new float[2];
    float palmEnergy = 0;

    float fingerEnergy = 0;
    int lastMovedFinger = 0;

    // We could use a class reprensenting positions (TODO readability)
    float fingerPosition[][] = new float[5][2];
    float fingerStiffnesses[] = {1f, 0.9f, 1f, 1.2f, 1.1f};
    float fingerTotalEnergies[] = new float[5];
    public float paramAdditionToFinger = .5F;
    public float paramFactorForFinger = 0.5F;
    private float paramFactorForFingerRepetition = 1.5F;
    private float paramFactorForPalm = 0.5F;
    
    private double paramPowerForFinger = 0.6;
    private float paramPowerForPalm = 0.6F;

    void resetFingers() {
        for (int i = 0; i < fingerPosition.length; i++) {
            for (int j = 0; j < fingerPosition[i].length; j++) {
                fingerPosition[i][j] = 0;
            }
        }
        fingerEnergy = 0f;
        lastMovedFinger = 0;
        palmPosition[0] = palmPosition[1] = 0;
    }

    public KlayHand() {
        resetFingers();
    }
    
    public void moveFinger(int finger, float[] newFingerPosition) {
        /*
        float[] previousFingerPosition = new float[2];
        previousFingerPosition[0] = fingerPosition[finger][0];
        previousFingerPosition[1] = fingerPosition[finger][1];
         */
        float[] previousFingerPosition = fingerPosition[finger];
        float dx = newFingerPosition[0] - previousFingerPosition[0];
        float dy = newFingerPosition[1] - previousFingerPosition[1];
        // Mass-spring-like formula for the energy of a move
        fingerEnergy = (float) Math.pow(dx * dx + dy * dy, paramPowerForFinger);
        fingerEnergy *= paramFactorForFinger * fingerStiffnesses[finger];
        // Energy for pressing down the key
        fingerEnergy += paramAdditionToFinger;
        // If the same finger is moved twice in a row, apply a penalty
        if (lastMovedFinger == finger) {
            fingerEnergy *= paramFactorForFingerRepetition;
        }
        
        previousFingerPosition = null;
        fingerPosition[finger][0] = newFingerPosition[0];
        fingerPosition[finger][1] = newFingerPosition[1];
        
        float[] previousPalmPosition = new float[]{palmPosition[0], palmPosition[1]};
        for (int i : new int[]{0,1}) {
            palmPosition[i] = 0;
            for (int f = 0; f < fingerPosition.length; f++) {
                palmPosition[i] += fingerPosition[f][i] * fingerStiffnesses[f];
            }
            palmPosition[i] /= 5.f;
        }
        
        dx = palmPosition[0] - previousPalmPosition[0];
        dy = palmPosition[1] - previousPalmPosition[1];
        palmEnergy = (float) Math.pow(dx * dx + dy * dy, paramPowerForPalm);
        palmEnergy *= paramFactorForPalm * palmStiffness;
        
        lastMovedFinger = finger;
    }
    
    // added to reuse klay processing
    public void reinit() {
        resetFingers();
    }
        
    public float currentCost() {
        return fingerEnergy + palmEnergy;
    }

}
