/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.klaylike;

import org.clavierdvorak.*;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author twilight
 */
public class KlayStatistic implements Statistic {

    public Map<Key, Integer> pressedKeys = Key.indexedMap();
    public Map<Key, Map<Key, Integer>> consecutivePressedKeys = Key.indexedMap();
    public Map<Key, Map<Key, Integer>> consecutiveSameFingerPressedKeys = Key.indexedMap();
    
    public int alternance;
    public Map<Key, Float> energyPerKey = Key.indexedMap();
    public float[] leftFingerEnergies = new float[5];
    public float[] rightFingerEnergies = new float[5];
    public int[] leftFingerCounts = new int[5];
    public int[] rightFingerCounts = new int[5];
    public float leftPalmEnergy = 0.f;
    public float rightPalmEnergy = 0.f;
    public int totalPressedSpecialKeys;
    
    public int totalPressedKeys;
    public float leftFingersEnergy;
    public float rightFingersEnergy;
    public float leftHandEnergy;
    public float rightHandEnergy;
    public float totalEnergy;
    public float totalEnergyPerKey;
    public float alternancePerKey;
	public int totalFingerCount;
    
    public void initMapsWithKeys(Collection<Key> keys) {
        for (Key key : keys) {
            pressedKeys.put(key, 0);
            energyPerKey.put(key, 0.f);
            consecutivePressedKeys.put(key, Key.<Integer>indexedMap());
            consecutiveSameFingerPressedKeys.put(key, Key.<Integer>indexedMap());
            for (Key secondKey : keys) {
                consecutivePressedKeys.get(key).put(secondKey, 0);
                consecutiveSameFingerPressedKeys.get(key).put(secondKey, 0);
            }
        }
    }

    public void processDerivedValues() {
        totalPressedKeys = sum(pressedKeys.values());
        leftFingersEnergy = sum(leftFingerEnergies);
        rightFingersEnergy = sum(rightFingerEnergies);
        leftHandEnergy = leftFingersEnergy + leftPalmEnergy;
        rightHandEnergy = rightFingersEnergy + rightPalmEnergy;
        totalEnergy = leftHandEnergy + rightHandEnergy;
        totalEnergyPerKey = totalEnergy / totalPressedKeys;//sum(energyPerKey.values());
        // Klay was calculating pressedKeyButSpecial (dead keys, ...) and dividing by it-1 here (TODO Klay fidelity)
        alternancePerKey = (float)alternance / (float)(totalPressedKeys-totalPressedSpecialKeys-1); // -1 because we can't alternate only from 2 key stokes
/*        totalConsecutiveSameFinger = 0;
        for (Map<Key, Integer> keycounts : consecutiveSameFingerPressedKeys.values()) {
        	for (Integer counts : keycounts.values()) {
        		totalConsecutiveSameFinger += counts.intValue(); 
        	}
        }*/
    }

    private float sum(float[] array) {
        float res = 0;
        for (float element : array) {
            res += element;
        }
        return res;
    }
    private int sum(Iterable<Integer> iterable) {
        int res = 0;
        for (int element : iterable) {
            res += element;
        }
        return res;
    }
    private float sum(Iterable<Float> iterable) {
        float res = 0;
        for (float element : iterable) {
            res += element;
        }
        return res;
    }

    public int getAlternanceCount() {
        return alternance;
    }

    public float getAlternancePerKey() {
        return alternancePerKey;
    }

    public Map<Key, Map<Key, Integer>> getConsecutivePressedKeys() {
        return consecutivePressedKeys;
    }

    public Map<Key, Map<Key, Integer>> getConsecutiveSameFingerPressedKeys() {
        return consecutiveSameFingerPressedKeys;
    }

    public Map<Key, Float> getEnergyPerKey() {
        return energyPerKey;
    }

    public float[] getLeftFingerEnergies() {
        return leftFingerEnergies;
    }

    public float getLeftFingersEnergy() {
        return leftFingersEnergy;
    }

    public float getLeftHandEnergy() {
        return leftHandEnergy;
    }

    public float getLeftPalmEnergy() {
        return leftPalmEnergy;
    }

    public Map<Key, Integer> getPressedKeys() {
        return pressedKeys;
    }

    public float[] getRightFingerEnergies() {
        return rightFingerEnergies;
    }

    public float getRightFingersEnergy() {
        return rightFingersEnergy;
    }

    public float getRightHandEnergy() {
        return rightHandEnergy;
    }

    public float getRightPalmEnergy() {
        return rightPalmEnergy;
    }

    public float getTotalEnergy() {
        return totalEnergy;
    }

    public float getTotalEnergyPerKey() {
        return totalEnergyPerKey;
    }

    public int getTotalPressedKeys() {
        return totalPressedKeys;
    }

    public int getTotalPressedSpecialKeys() {
        return totalPressedSpecialKeys;
    }
    
    //////////////
    
    
}
