/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.clavierdvorak.ui.model.Corpus;
import org.clavierdvorak.ui.model.KeyboardComparatorModel;
import org.clavierdvorak.ui.model.Layout;
import org.clavierdvorak.ui.view.TopLevelView;

/**
 *
 * @author twilight
 */
public class Main {
    
    public static void main(String[] args) {
        final KeyboardComparatorModel model = new KeyboardComparatorModel();
        readLayoutInitialisationData(model);
        readCorpusInitialisationData(model);
        /*
        addLocalCorpus(model, "/tmp/johnix/mail", "ISO-8859-1");
        addLocalCorpus(model, "/tmp/johnix/autonomy.txt", "ISO-8859-1");
        addLocalCorpus(model, "/tmp/johnix/prog", "ISO-8859-1");
        addLocalCorpus(model, "/tmp/johnix/swann10.txt", "ISO-8859-1");
        addLocalCorpus(model, "clean-M355.txt", "UTF-8");
        addLocalCorpus(model, "corpus-klay-clean.txt", "UTF-8");
        addLocalCorpus(model, "romans.txt", "UTF-8");
         */
        TopLevelView view = new TopLevelView(model);
        JFrame f = new JFrame("test");
        f.setPreferredSize(new Dimension(800, 600));
        f.setContentPane(view);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();
        f.setVisible(true);
        tryToImportCorpusFromWiki(model, "http://www.clavier-dvorak.org/wiki/Corpus");
    }

    private static void addLocalCorpus(KeyboardComparatorModel model, final String path, final String encoding) {
        try {
            new FileInputStream(path).close();
        } catch (IOException ex) {
            System.out.println("Problem opening local file '"+path+"' ... ignoring this corpus");
            return;
        }
        model.addCorpus(new Corpus() {

            public String getName() {
                return path;
            }

            @Override
            public String toString() {
                return path;
            }

            public String getEncoding() {
                return encoding;
            }

            public InputStream getContent() {
                try {
                    return new FileInputStream(path);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
            });
    }
    
    private static Layout l(final String resourcePath, final String name, final String comment) {
        return new Layout() {
            public String getName() {
                return name;
            }
            public InputStream getContent() {
                return Main.class.getClassLoader().getResourceAsStream(resourcePath);
            }

            @Override
            public String toString() {
                return name;
            }
            
        };
    }
    public static void readLayoutInitialisationData(KeyboardComparatorModel model) {
        final String resourceBasePath = "org/clavierdvorak/ui/resource";
        ResourceBundle bundle = ResourceBundle.getBundle(resourceBasePath+"/layouts"); // NOI18N
        for (String fileName : bundle.getString("layouts").split(" ")) {
            final String resourcePath = resourceBasePath + "/" + fileName;
            final InputStream stream = Main.class.getClassLoader().getResourceAsStream(resourcePath);
            if (null != stream) {
                String name;
                try {
                    name = bundle.getString("name___" + fileName);
                } catch (MissingResourceException e) {
                    //TODO should log missing name
                    continue;
                }
                String comment = "";
                try {
                    comment = bundle.getString("comment___" + fileName);
                } catch (MissingResourceException e) {
                    // ignored because optional
                }
                model.addLayout(l(resourcePath, name, comment));
                try {
                    stream.close();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                //TODO should log
            }
        }
    }
    private static Corpus c(final String resourcePath, final String name, final String encoding) {
        return new Corpus() {
            public String getName() {
                return name;
            }
            public InputStream getContent() {
                return Main.class.getClassLoader().getResourceAsStream(resourcePath);
            }

            @Override
            public String toString() {
                return name;
            }

            public String getEncoding() {
                return encoding;
            }
        };
    }
    public static void readCorpusInitialisationData(KeyboardComparatorModel model) {
        final String resourceBasePath = "org/clavierdvorak/ui/resource";
        ResourceBundle bundle = ResourceBundle.getBundle(resourceBasePath+"/corpora"); // NOI18N
        for (String fileName : bundle.getString("corpora").split(" ")) {
            final String resourcePath = resourceBasePath + "/" + fileName;
            final InputStream stream = Main.class.getClassLoader().getResourceAsStream(resourcePath);
            if (null != stream) {
                String name;
                try {
                    name = bundle.getString("name___" + fileName);
                } catch (MissingResourceException e) {
                    //TODO should log missing name
                    continue;
                }
                String encoding = "utf-8";
                try {
                    String readEncoding = bundle.getString("encoding___" + fileName);
                    Charset.forName(readEncoding);
                    encoding = readEncoding;
                } catch (MissingResourceException e) {
                    // ignored because defaults to utf-8
                } catch (IllegalCharsetNameException e) {
                    //TODO sould log
                } catch (UnsupportedCharsetException e) {
                    //TODO sould log
                }
                model.addCorpus(c(resourcePath, name, encoding));
                try {
                    stream.close();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                //TODO should log
            }
        }
    }

    private static void addCorpus(final KeyboardComparatorModel model, final Corpus corpus) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                model.addCorpus(corpus);
            }
        });
    }
    
    private static void tryToImportCorpusFromWiki(final KeyboardComparatorModel model, final String wikiUrl) {
        // <td><a href="http://clement.chassagne.free.fr/public/dvorak-fr/corpus/extrait_001.txt" class="external autonumber" title="http://clement.chassagne.free.fr/public/dvorak-fr/corpus/extrait_001.txt" rel="nofollow">[1]</a></td><td>968657375e4c907d20bfd499539f98fd</td><td>extrait de quelques articles de www.lemonde.fr</td><td>54 954</td><td>ISO 8859-15
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    String wiki = getUrlContent(wikiUrl, "<td><a href=.*");
                    Pattern pattern = Pattern.compile("<td><a href=\"([^\"]*)\".*</td><td>.*</td><td>(.*)</td><td>([^<]+).*");
                    int i = 0;
                    for (String line : wiki.split("\n")) {
                        i++;
                        Matcher matcher = pattern.matcher(line);
                        if (matcher.matches()) {
                            final String url = matcher.group(1);
                            String tmpSize = matcher.group(2);
                            try {
                                int power = 0;
                                float size = Integer.parseInt(matcher.group(2).replaceAll("\\s", ""));
                                while (size > 1000) {
                                    size /= 1000;
                                    power++;
                                }
                                String powerString = new String[]{"", "k", "M", "G", "T"}[power];
                                tmpSize = String.format(Locale.US, "%.1f%s", size, powerString);
                                if (power >= 2) {
                                    tmpSize = "! " + tmpSize + " !";
                                }
                            } catch (Exception e) {
                            }
                            final String size = tmpSize;
                            final String encoding = matcher.group(3);
                            final int id = i;
                            addCorpus(model,new Corpus() {
                                public String getName() {
                                    return "[wiki]["+size+"] "+url.replaceAll(".*/", "");
                                }
                                public String getEncoding() {
                                    return encoding;
                                }
                                public InputStream getContent() {
                                    try {
                                        return new URL(url).openStream();
                                    } catch (IOException ex) {
                                        System.err.println("Error opening Wiki corpus "+id);
                                        return null;
                                    }
                                }

                                @Override
                                public String toString() {
                                    return getName();
                                }
                            });
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    private static String getUrlContent(String wikiUrl, String lineFilter) throws MalformedURLException, IOException {
        StringBuilder res = new StringBuilder();
        InputStream input = new URL(wikiUrl).openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, "UTF-8"));
        String line = reader.readLine();
        while (line != null) {
            if (line.matches(lineFilter)) {
                res.append(line).append("\n");
            }
            line = reader.readLine();
        }
        reader.close();
        return res.toString();
    }

}
