/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.model;

import java.io.InputStream;

/**
 *
 * @author twilight
 */
public interface Corpus {

    public String getName();
    public String getEncoding();
    public InputStream getContent();
}
