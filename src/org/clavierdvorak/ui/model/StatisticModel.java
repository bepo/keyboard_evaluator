/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import javax.swing.SwingWorker;
import org.clavierdvorak.CorpusStatistic;
import org.clavierdvorak.keyselector.EmptyKeySelector;
import org.clavierdvorak.EmptyTyper;
import org.clavierdvorak.Key;
import org.clavierdvorak.KeySelector;
import org.clavierdvorak.Typer;
import org.clavierdvorak.keyselector.ClassicKeyboardKeySelector;
import org.clavierdvorak.klaylike.KlayStatistic;
import org.clavierdvorak.klaylike.KlayTyper;

/**
 *
 * @author twilight
 */
public class StatisticModel {

    private Layout layout;
    private Corpus corpus;
    private KeyboardComparatorModel parent;
    private SwingWorker worker = null;
    private KlayStatistic statistic = null;
    private CorpusStatistic corpusStatistic = null;

    public StatisticModel(Layout layout, Corpus corpus, KeyboardComparatorModel topLevelModel) {
        this.layout = layout;
        this.corpus = corpus;
        this.parent = topLevelModel;
        this.worker = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                return processStat();
            }
            @Override
            protected void done() {
                parent.rebuildTables();
            }
        };
        worker.execute();
    }

    public CorpusStatistic getCorpusStatistic() {
        return corpusStatistic;
    }

    public KlayStatistic getStatistic() {
        return statistic;
    }

    public boolean isProcessing() {
        return corpusStatistic == null;
    }
    
    private Object processStat() throws InterruptedException, IOException {
        KeySelector keySelector = new ClassicKeyboardKeySelector(layout.getContent());
        Typer typer = new KlayTyper();
        Reader characterReader = new BufferedReader(new InputStreamReader(corpus.getContent(), corpus.getEncoding()));
        Iterator<Key> keySequence = keySelector.getKeySequenceFor(characterReader);
        statistic = (KlayStatistic) typer.typeKeySequence(keySequence);
        corpusStatistic = keySelector.getCorpusStatistic();
        characterReader.close();
        return this;
    }

    public Object getModel() {
        return this;
    }

    public static CorpusStatistic corpusStatistic(Corpus corpus) throws IOException {
        KeySelector keySelector = new EmptyKeySelector();
        Typer typer = new EmptyTyper();
        Reader characterReader = new BufferedReader(new InputStreamReader(corpus.getContent(), corpus.getEncoding()));
        Iterator<Key> keySequence = keySelector.getKeySequenceFor(characterReader);
        typer.typeKeySequence(keySequence);
        CorpusStatistic corpusStatistic = keySelector.getCorpusStatistic();
        characterReader.close();
        return corpusStatistic;
    }
    

}
