/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.model;

import java.io.InputStream;

/**
 *
 * @author twilight
 */
public interface Layout {

    public String getName();
    public InputStream getContent();
}
