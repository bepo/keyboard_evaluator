/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author twilight
 */
public class KeyboardComparatorModel {

    private DefaultListModel/*<Layout>*/ layouts = new DefaultListModel();
    private DefaultListModel/*<Corpus>*/ corpus = new DefaultListModel();
    private DefaultTableModel results = new DefaultTableModel();
    private DefaultListSelectionModel selectedLayouts = new DefaultListSelectionModel();
    private DefaultListSelectionModel selectedCorpus = new DefaultListSelectionModel();
  
        
    private Map<Layout, Map<Corpus, StatisticModel>> stats = new Hashtable<Layout, Map<Corpus, StatisticModel>>();
    
    public KeyboardComparatorModel() {
        ListSelectionListener l = new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent arg0) {
                rebuildTables();
            }
        };
        selectedLayouts.addListSelectionListener(l);
        selectedCorpus.addListSelectionListener(l);
    }

    public void addLayout(Layout layout) {
        layouts.addElement(layout);
    }

    public void addCorpus(Corpus c) {
        corpus.addElement(c);
    }

    public ListModel getCorpus() {
        return corpus;
    }

    public ListModel getLayouts() {
        return layouts;
    }

    public TableModel getResults() {
        return results;
    }
        
    public ListSelectionModel getSelectedLayouts() {
        return selectedLayouts;
    }


    /*package*/ synchronized void rebuildTables() {
        List<Corpus> selectedCorpusList = selected(corpus, selectedCorpus);
        boolean preventColumnUpdate = results.getColumnCount() - 1 == selectedCorpusList.size();
        if (preventColumnUpdate) {
            int i = 1;
            for (Corpus c : selectedCorpusList) {
                if (!c.toString().equals(results.getColumnName(i++))) {
                    preventColumnUpdate = false;
                    break;
                }
            }
        }
        if (!preventColumnUpdate) {
            Vector cols = new Vector();
            int i = 0;
            cols.add(i++, "Layout");
            for (Corpus c : selectedCorpusList) {
                cols.add(i++, c);
            }
            results.setColumnIdentifiers(cols);
        }
        results.setRowCount(0);
        List<Layout> selectedLayoutsList = selected(layouts, selectedLayouts);
        for (Layout layout : selectedLayoutsList) {
            results.addRow(row(layout, selectedCorpusList));
        }
    }

    private Vector<?> row(Layout layout, Iterable<Corpus> selectedCorpusList) {
        Vector res = new Vector();
        res.add(layout);
        for (Corpus c : selectedCorpusList) {
            res.add(getStat(layout, c));
        }
        return res;
    }
    private synchronized Object getStat(Layout l, Corpus c) {
        Map<Corpus, StatisticModel> partial = stats.get(l);
        if (partial == null) {
            partial = new Hashtable<Corpus, StatisticModel>();
            stats.put(l, partial);
        }
        StatisticModel res = partial.get(c);
        if (res == null) {
            res = new StatisticModel(l, c, this);
            partial.put(c, res);
        }
        return res.getModel();
    }

    public ListSelectionModel getSelectedCorpus() {
        return selectedCorpus;
    }

    public static List<Corpus> selected(ListModel list, ListSelectionModel selection) {
        return selected((DefaultListModel) list, (DefaultListSelectionModel) selection);
    }

    private static <T> List<T> selected(DefaultListModel model, DefaultListSelectionModel selectionModel) {
        List<T> res = new ArrayList<T>();
        for (int i = selectionModel.getMinSelectionIndex(); i <= selectionModel.getMaxSelectionIndex(); i++) {
            if (selectionModel.isSelectedIndex(i)) {
                res.add((T)model.get(i));
            }
        }
        return res;
    }
 
}
