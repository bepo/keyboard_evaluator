/*
 * TopLevelView.java
 *
 * Created on January 19, 2008, 12:25 AM
 */

package org.clavierdvorak.ui.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JToggleButton.ToggleButtonModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableModel;
import org.clavierdvorak.CorpusStatistic;
import org.clavierdvorak.ui.model.Corpus;
import org.clavierdvorak.ui.model.KeyboardComparatorModel;
import org.clavierdvorak.ui.model.Layout;
import org.clavierdvorak.ui.model.StatisticModel;

/**
 *
 * @author  twilight
 */
public class TopLevelView extends javax.swing.JPanel {
    
    private KeyboardComparatorModel model;
    private static enum Adding {CORPUS, LAYOUT};
    private Adding adding = null;
    private StatisticCellRenderer statisticCellRender = new StatisticCellRenderer();
    private ButtonModel corpusFusionModel = new ToggleButtonModel();
    private TableModel fusionModel;

    public TopLevelView(final KeyboardComparatorModel model) {
        this.model = model;
        initComponents();
        resultTable.setDefaultRenderer(Object.class, statisticCellRender);
        fusionModel = new CorpusFusionViewModel(model.getResults());
        formatComboBox.setRenderer(new DefaultListCellRenderer(){
            @Override
            public Component getListCellRendererComponent(JList arg0, Object arg1, int arg2, boolean arg3, boolean arg4) {
                if (null != arg1) {
                    String localized = "";
                    ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle");
                    String prefix = TopLevelView.class.getName().replaceAll("^.*\\.", "") + ".";
                    String separator = " ";
                    try {
                        separator = bundle.getString(prefix + "separator");
                    } catch (Exception ex) {
                    }
                    for (String element : arg1.toString().split("\\|")) {
                        try {
                            localized += bundle.getString(prefix + element);
                        } catch (Exception ex) {
                            localized += "???";
                        }
                        localized += separator;
                    }
                    localized = localized.substring(0, localized.length() - separator.length());
                    return super.getListCellRendererComponent(arg0, localized, arg2, arg3, arg4);
                } else {
                    return super.getListCellRendererComponent(arg0, "...", arg2, arg3, arg4);
                }
            } 
        });
        statisticCellRender.getMessageFormatModel().addListDataListener(new ListDataListener() {
            public void intervalAdded(ListDataEvent arg0) {
            }
            public void intervalRemoved(ListDataEvent arg0) {
           }
            public void contentsChanged(ListDataEvent arg0) {
                resultTable.invalidate();
                resultTable.repaint();
           }
        });
        corpusFusionModel.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (corpusFusionModel.isSelected()) {
                    if (resultTable.getModel() != fusionModel) {
                        resultTable.setModel(fusionModel);
                        formatComboBox.setEnabled(false);
                    }
                } else {
                    if (resultTable.getModel() != model.getResults()) {
                        resultTable.setModel(model.getResults());
                        formatComboBox.setEnabled(true);
                    }
                }
            }
        });
    }

    public KeyboardComparatorModel getModel() {
        return model;
    }
    
    /** Creates new form TopLevelView */
    TopLevelView() {
        initComponents();
    }

    private void addGeneric(Adding a, String prop) {
        adding = a;
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle"); // NOI18N
        nameLabel.setText(bundle.getString(prop)); // NOI18N
        this.removeAll();
        this.add(adderPanel, BorderLayout.CENTER);
        nameTextField.requestFocus();
        nameTextField.setText("");
        /*
        nameTextField.setSelectionStart(0);
        nameTextField.setSelectionEnd(nameTextField.getText().length());
         */
        contentTextArea.setText("");
        adderPanel.repaint();
        this.revalidate();
    }
    
    private void addCorpusFromFileChooser(final String encoding) {
        JFileChooser chooser = new JFileChooser(".");
        chooser.setMultiSelectionEnabled(true);
        if (JFileChooser.APPROVE_OPTION == chooser.showOpenDialog(this)) {
            for (final File file : chooser.getSelectedFiles()) {
                model.addCorpus(new Corpus() {

                    public String getName() {
                        try {
                            return file.getCanonicalPath() + " [" + encoding + "]";
                        } catch (IOException ex) {
                            Logger.getLogger(TopLevelView.class.getName()).log(Level.SEVERE, null, ex);
                            return super.toString();
                        }
                    }

                    public InputStream getContent() {
                        try {
                            return new FileInputStream(file);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(TopLevelView.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                    }

                    public String getEncoding() {
                        return encoding;
                    }

                    @Override
                    public String toString() {
                        return getName();
                    }
                });
            }
        }
    }
    
    private CorpusStatistic statistic(Corpus c) throws IOException {
        return StatisticModel.corpusStatistic(c);
    }
    private final String ch(char c) {
        if (c == '"') {
            return "\"\"";
        } else {
            return "\""+c+"\"";
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        adderPanel = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        nameTextField = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        contentTextArea = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        layoutsList = new javax.swing.JList();
        addLayoutButton = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        corpusList = new javax.swing.JList();
        addCorpusButton = new javax.swing.JButton();
        addIsoCorpusButton = new javax.swing.JButton();
        addIsoCorpusButton1 = new javax.swing.JButton();
        corpusStatToClipboardButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        formatComboBox = new javax.swing.JComboBox();
        corpusFusionCheckBox = new javax.swing.JCheckBox();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle"); // NOI18N
        nameLabel.setText(bundle.getString("TopLevelView.nameLabel.text")); // NOI18N

        nameTextField.setText(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle").getString("TopLevelView.nameTextField.text"), new Object[] {})); // NOI18N

        contentTextArea.setColumns(20);
        contentTextArea.setRows(5);
        jScrollPane4.setViewportView(contentTextArea);

        jButton1.setText(bundle.getString("TopLevelView.jButton1.text")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizeAdd(evt);
            }
        });

        jButton2.setText(bundle.getString("TopLevelView.jButton2.text")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelAdd(evt);
            }
        });

        jButton3.setText(bundle.getString("TopLevelView.jButton3.text")); // NOI18N
        jButton3.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizeAdd(evt);
            }
        });

        javax.swing.GroupLayout adderPanelLayout = new javax.swing.GroupLayout(adderPanel);
        adderPanel.setLayout(adderPanelLayout);
        adderPanelLayout.setHorizontalGroup(
            adderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, adderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(adderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .addGroup(adderPanelLayout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 425, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, adderPanelLayout.createSequentialGroup()
                        .addComponent(nameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nameTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        adderPanelLayout.setVerticalGroup(
            adderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(adderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(adderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLabel)
                    .addComponent(nameTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(adderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        setLayout(new java.awt.BorderLayout());

        splitPane.setDividerLocation(150);
        splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitPane.setOneTouchExpandable(true);

        jSplitPane2.setDividerLocation(200);
        jSplitPane2.setResizeWeight(0.5);

        jLabel1.setText(bundle.getString("TopLevelView.jLabel1.text")); // NOI18N

        layoutsList.setModel(model.getLayouts());
        layoutsList.setSelectionModel(model.getSelectedLayouts());
        jScrollPane1.setViewportView(layoutsList);

        addLayoutButton.setText(bundle.getString("TopLevelView.addLayoutButton.text")); // NOI18N
        addLayoutButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        addLayoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addLayout(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addGap(29, 29, 29)
                .addComponent(addLayoutButton))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(addLayoutButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
        );

        jSplitPane2.setLeftComponent(jPanel3);

        jLabel2.setText(bundle.getString("TopLevelView.jLabel2.text")); // NOI18N

        corpusList.setModel(model.getCorpus());
        corpusList.setSelectionModel(model.getSelectedCorpus());
        jScrollPane2.setViewportView(corpusList);

        addCorpusButton.setText(bundle.getString("TopLevelView.addCorpusButton.text")); // NOI18N
        addCorpusButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        addCorpusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCorpus(evt);
            }
        });

        addIsoCorpusButton.setText(bundle.getString("TopLevelView.addIsoCorpusButton.text")); // NOI18N
        addIsoCorpusButton.setToolTipText(bundle.getString("TopLevelView.addIsoCorpusButton.toolTipText")); // NOI18N
        addIsoCorpusButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        addIsoCorpusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIsoCorpus(evt);
            }
        });

        addIsoCorpusButton1.setText(bundle.getString("TopLevelView.addIsoCorpusButton1.text")); // NOI18N
        addIsoCorpusButton1.setToolTipText(bundle.getString("TopLevelView.addIsoCorpusButton1.toolTipText")); // NOI18N
        addIsoCorpusButton1.setMargin(new java.awt.Insets(0, 0, 0, 0));
        addIsoCorpusButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUtfCorpus(evt);
            }
        });

        corpusStatToClipboardButton.setText(bundle.getString("TopLevelView.corpusStatToClipboardButton.text")); // NOI18N
        corpusStatToClipboardButton.setToolTipText(bundle.getString("TopLevelView.corpusStatToClipboardButton.toolTipText")); // NOI18N
        corpusStatToClipboardButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        corpusStatToClipboardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corpusStatToClipboard(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(corpusStatToClipboardButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addIsoCorpusButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addIsoCorpusButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addCorpusButton))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(addCorpusButton)
                    .addComponent(addIsoCorpusButton)
                    .addComponent(addIsoCorpusButton1)
                    .addComponent(corpusStatToClipboardButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
        );

        jSplitPane2.setRightComponent(jPanel4);

        splitPane.setTopComponent(jSplitPane2);

        jLabel3.setText(bundle.getString("TopLevelView.jLabel3.text")); // NOI18N

        resultTable.setModel(model.getResults());
        jScrollPane3.setViewportView(resultTable);

        formatComboBox.setModel(this.statisticCellRender.getMessageFormatModel());

        corpusFusionCheckBox.setModel(corpusFusionModel);
        corpusFusionCheckBox.setText(bundle.getString("TopLevelView.corpusFusionCheckBox.text")); // NOI18N
        corpusFusionCheckBox.setToolTipText(bundle.getString("TopLevelView.corpusFusionCheckBox.toolTipText")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(corpusFusionCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(formatComboBox, 0, 406, Short.MAX_VALUE))
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 557, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(formatComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(corpusFusionCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE))
        );

        splitPane.setRightComponent(jPanel1);

        add(splitPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void finalizeAdd(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizeAdd
        final String name = nameTextField.getText();
        final String content = contentTextArea.getText();
        switch (adding) {
            case CORPUS:
                model.addCorpus(new Corpus() {
                    public String getName() {
                        return name + "*";
                    }
                    public InputStream getContent() {
                        try {
                            return new ByteArrayInputStream(content.getBytes("utf-8"));
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(TopLevelView.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                    }
                    public String getEncoding() {
                        return "utf-8";
                    }

                    @Override
                    public String toString() {
                        return getName();
                    }
                });
                break;
            case LAYOUT:
                model.addLayout(new Layout() {
                    public String getName() {
                        return name + "*";
                    }
                    public InputStream getContent() {
                        return new ByteArrayInputStream(content.getBytes());
                    }
 
                    @Override
                    public String toString() {
                        return getName();
                    }
               });
                break;
        }
        cancelAdd(evt);
    }//GEN-LAST:event_finalizeAdd

    private void cancelAdd(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelAdd
        this.removeAll();
        this.setLayout(new BorderLayout());
        this.add(splitPane, BorderLayout.CENTER);
        splitPane.repaint();
        this.revalidate();
    }//GEN-LAST:event_cancelAdd

    private void addLayout(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addLayout
        addGeneric(Adding.LAYOUT, "TopLevelView.nameLabel.text.layout");
    }//GEN-LAST:event_addLayout

    private void addCorpus(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCorpus
        addGeneric(Adding.CORPUS, "TopLevelView.nameLabel.text.corpus");
    }//GEN-LAST:event_addCorpus

    private void addIsoCorpus(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIsoCorpus
        addCorpusFromFileChooser("ISO-8859-1");
}//GEN-LAST:event_addIsoCorpus

    private void addUtfCorpus(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUtfCorpus
        addCorpusFromFileChooser("UTF-8");
}//GEN-LAST:event_addUtfCorpus

    private void corpusStatToClipboard(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_corpusStatToClipboard
        String sep = ";";
        StringBuilder stat = new StringBuilder();
        List<CorpusStatistic> corpusStats = new ArrayList<CorpusStatistic>();
        Map<CorpusStatistic, String> corpusNames = new HashMap();
        {
            List<Corpus> corpus = KeyboardComparatorModel.selected(model.getCorpus(), model.getSelectedCorpus());
            for (Corpus c : corpus) {
                try {
                    final CorpusStatistic statistic = statistic(c);
                    corpusStats.add(statistic);
                    corpusNames.put(statistic, c.getName());
                } catch (IOException e) {
                }
            }
        }
        {
            TreeSet<Character> characters = new TreeSet<Character>();
            {
                stat.append("Character");
                for (CorpusStatistic s : corpusStats) {
                    stat.append(sep).append(corpusNames.get(s));
                    characters.addAll(s.getReadCharacters().keySet());
                }
                stat.append(sep).append(sep).append("Digram1").append(sep).append("Digram2");
                for (CorpusStatistic s : corpusStats) {
                    stat.append(sep).append(corpusNames.get(s));
                    characters.addAll(s.getReadCharacters().keySet());
                }
            }
            stat.append("\n");
            for (Character c : characters) {
                stat.append(ch(c));
                for (CorpusStatistic s : corpusStats) {
                    final Integer value = s.getReadCharacters().get(c);
                    stat.append(sep).append(value == null ? 0 : value);
                }
                stat.append("\n");
            }
            for (Character c1 : characters) {
                for (Character c2 : characters) {
                    for (CorpusStatistic s : corpusStats) {
                        stat.append(sep);
                    }
                    stat.append(sep).append(sep).append(ch(c1)).append(sep).append(ch(c2));
                    for (CorpusStatistic s : corpusStats) {
                        final Map<Character, Integer> intermediate = s.getReadDigrams().get(c1);
                        if (intermediate == null) {
                            stat.append(sep).append(0);
                        } else {
                            final Integer value = intermediate.get(c2);
                            stat.append(sep).append(value == null ? 0 : value);
                        }
                    }
                    stat.append("\n");
                }
            }
        }
        final StringSelection stringSelection = new StringSelection(stat.toString());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, stringSelection);
    }//GEN-LAST:event_corpusStatToClipboard
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addCorpusButton;
    private javax.swing.JButton addIsoCorpusButton;
    private javax.swing.JButton addIsoCorpusButton1;
    private javax.swing.JButton addLayoutButton;
    private javax.swing.JPanel adderPanel;
    private javax.swing.JTextArea contentTextArea;
    private javax.swing.JCheckBox corpusFusionCheckBox;
    private javax.swing.JList corpusList;
    private javax.swing.JButton corpusStatToClipboardButton;
    private javax.swing.JComboBox formatComboBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JList layoutsList;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JTable resultTable;
    private javax.swing.JSplitPane splitPane;
    // End of variables declaration//GEN-END:variables
    
}
