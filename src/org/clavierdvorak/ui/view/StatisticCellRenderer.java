/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import org.clavierdvorak.CorpusStatistic;
import org.clavierdvorak.klaylike.KlayStatistic;
import org.clavierdvorak.ui.model.StatisticModel;
import org.clavierdvorak.ui.view.CorpusFusionViewModel.InProgress;

/**
 *
 * @author twilight
 */
public class StatisticCellRenderer extends JPanel implements TableCellRenderer {

    private DefaultComboBoxModel messageFormatModel = new DefaultComboBoxModel();

    public DefaultComboBoxModel getMessageFormatModel() {
        return messageFormatModel;
    }

    public StatisticCellRenderer() {
        super(new GridLayout(1, 0));
        this.setOpaque(true);
        this.setBackground(Color.WHITE);// should use ui defaults for lists
        ResourceBundle bundle = java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle");
        String prefix = TopLevelView.class.getName().replaceAll("^.*\\.", "") + ".";
        try {
            for (String element : bundle.getString(prefix + "nofusion.choices").split("\\|_\\|")) {
                messageFormatModel.addElement(element);
            }
        } catch (Exception e) {
            // defaults to something
            messageFormatModel.addElement("$A|$E");
            messageFormatModel.addElement("$E|$Et");
            messageFormatModel.addElement("$A|$N");
        }
        processing.setOpaque(true);
        processing.setBackground(Color.YELLOW);
    }
    
    private JLabel processing = new JLabel("");
    private JLabel various = new JLabel("");
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (value instanceof StatisticModel) {
            StatisticModel stat = (StatisticModel) value;
            if (stat.isProcessing()) {
                processing.setText("...");
                return processing;
            } else {
                buildMessage(stat.getCorpusStatistic(), stat.getStatistic());
                return this;
            }
        } else if (value instanceof CorpusFusionViewModel.InProgress) {
            CorpusFusionViewModel.InProgress wip = (InProgress) value;
            processing.setText(wip.present+"/"+wip.total);
            return processing;
        } else {
            various.setText(value.toString());
            return various;
        }
    }

    private void addLabel(Object o) {
        JLabel lab = new JLabel(o.toString());
        lab.setHorizontalAlignment(JLabel.CENTER);
        this.add(lab);
    }

    
    private void buildMessage(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
        this.removeAll();
        this.setLayout(new GridLayout(1, 0));
        for (String element : messageFormatModel.getSelectedItem().toString().split("\\|")) {
            addLabel(getElement(corpusStatistic, statistic, element));
        }
    }
    
    public static Number getElement(CorpusStatistic corpusStatistic, KlayStatistic statistic, String element) {
        StatisticWildcardResolver resolver = StatisticWildcardResolver.getResolver(element);
        if (resolver != null) {
            resolver.init();
            resolver.cumulate(corpusStatistic, statistic);
            return resolver.getResult();
        } else {
            return -111;
        }
//        if (element.equals("$At")) {
//            return statistic.getAlternance();
//        } else if (element.equals("$A")) {
//            return statistic.getAlternancePerKey();
//        } else if (element.equals("$Et")) {
//            return statistic.getTotalEnergy();
//        } else if (element.equals("$E")) {
//            return statistic.getTotalEnergyPerKey();
//        } else if (element.equals("$N")) {
//            return statistic.getTotalPressedKeys();
//        } else {
//            return null;
//        }
    }

}
