/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.view;

import java.util.HashMap;
import java.util.Map;
import org.clavierdvorak.CorpusStatistic;
import org.clavierdvorak.klaylike.KlayStatistic;

/**
 *
 * @author twilight
 */
public abstract class StatisticWildcardResolver {
    
    String description;

    public StatisticWildcardResolver(String description) {
        this.description = description;
    }

    public abstract void init();
    public abstract void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic);
    public abstract Number getResult();
    
    private static Map<String, StatisticWildcardResolver> resolvers = new HashMap<String, StatisticWildcardResolver>() {{
            put("$A", new StatisticWildcardResolver("Alternance") {

                int alternance;
                int possibilities;
                @Override
                public void init() {
                    alternance = 0;
                    possibilities = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    alternance += statistic.getAlternanceCount();
                    possibilities += statistic.getTotalPressedKeys()-statistic.getTotalPressedSpecialKeys()-1;
                }

                @Override
                public Number getResult() {
                    return (float)alternance/(float)possibilities;
                }

            });
            put("$At", new StatisticWildcardResolver("Alternance Count") {

                int alternance;
                @Override
                public void init() {
                    alternance = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    alternance += statistic.getAlternanceCount();
                }

                @Override
                public Number getResult() {
                    return alternance;
                }

            });
            put("$E", new StatisticWildcardResolver("Energy") {

                float energy;
                int keys;
                @Override
                public void init() {
                    energy = 0;
                    keys = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    energy += statistic.getTotalEnergy();
                    keys += statistic.getTotalPressedKeys();
                }

                @Override
                public Number getResult() {
                    return (float)energy/(float)keys;
                }

            });
            put("$Et", new StatisticWildcardResolver("Total Energy") {

                float energy;
                @Override
                public void init() {
                    energy = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    energy += statistic.getTotalEnergy();
                }

                @Override
                public Number getResult() {
                    return energy;
                }

            });
        put("$N", new StatisticWildcardResolver("Total Keys") {

                int keys;
                @Override
                public void init() {
                    keys = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    keys += statistic.getTotalPressedKeys();
                }

                @Override
                public Number getResult() {
                    return keys;
                }

            });

        put("$Nspe",  new StatisticWildcardResolver("Total Keys") {

                int keys;
                @Override
                public void init() {
                    keys = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    keys += statistic.getTotalPressedSpecialKeys();
                }

                @Override
                public Number getResult() {
                    return keys;
                }

            });

        put("$Nnospe",  new StatisticWildcardResolver("Total Keys") {

                int keys;
                @Override
                public void init() {
                    keys = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    keys += statistic.getTotalPressedKeys() - statistic.getTotalPressedSpecialKeys();
                }

                @Override
                public Number getResult() {
                    return keys;
                }

            });

        put("$CN",  new StatisticWildcardResolver("Total Keys") {

                int chars;
                @Override
                public void init() {
                    chars = 0;
                }
                @Override
                public void cumulate(CorpusStatistic corpusStatistic, KlayStatistic statistic) {
                    chars += corpusStatistic.getTotalReadCharacters();
                }

                @Override
                public Number getResult() {
                    return chars;
                }

            });
        }};
                
    public static StatisticWildcardResolver getResolver(String wildcard) {
        return resolvers.get(wildcard);
    }

}
