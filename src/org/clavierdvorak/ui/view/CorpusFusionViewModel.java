/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.ui.view;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.clavierdvorak.ui.model.StatisticModel;

/**
 *
 * @author twilight
 */
class CorpusFusionViewModel extends AbstractTableModel implements TableModelListener {

    TableModel underlyingModel;
    String[] columns;
    
    public CorpusFusionViewModel(TableModel results) {
        underlyingModel = results;
        columns = java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle").getString(TopLevelView.class.getName().replaceAll("^.*\\.", "") + ".fusion.columns").split("\\|");
        underlyingModel.addTableModelListener(this);
    }

    public int getRowCount() {
        return underlyingModel.getRowCount();
    }

    public int getColumnCount() {
        return columns.length+1;
    }

    @Override
    public String getColumnName(int index) {
        if (index == 0) {
            return "Layout";
        } else {
            try {
                return java.util.ResourceBundle.getBundle("org/clavierdvorak/ui/view/Bundle").getString(TopLevelView.class.getName().replaceAll("^.*\\.", "") + "." + columns[index - 1]);
            } catch (Exception e) {
                return columns[index-1];
            }
        }
    }

    private InProgress noCorpus = new InProgress(0, 0);
    public Object getValueAt(int row, int col) {
        if (col == 0) {
            return underlyingModel.getValueAt(row, col);
        } else if (underlyingModel.getColumnCount() == 1) {
            return noCorpus;
        } else {
            int total = underlyingModel.getColumnCount() - 1;
            int present = total;
            for (int i = 1; i < underlyingModel.getColumnCount(); i++) {
                if (((StatisticModel)underlyingModel.getValueAt(row, i)).isProcessing()) {
                    present--;
                }
            }
            if (present == total) {
                StatisticWildcardResolver resolver = StatisticWildcardResolver.getResolver(columns[col-1]);
                if (resolver != null) {
                    resolver.init();
                    for (int i = 1; i < underlyingModel.getColumnCount(); i++) {
                        StatisticModel stat = (StatisticModel) underlyingModel.getValueAt(row, i);
                        resolver.cumulate(stat.getCorpusStatistic(), stat.getStatistic());
                    }
                    return resolver.getResult();
                } else {
                    return "-111";
                }
            } else {
                return new InProgress(present, total);
            }
        }
    }

    static class InProgress {
        int present;
        int total;

        private InProgress(int present, int total) {
            this.present = present;
            this.total = total;
        }
        
    }
    
    public void tableChanged(TableModelEvent e) {
        this.fireTableDataChanged();
    }

}
