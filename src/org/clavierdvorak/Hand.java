/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author twilight
 */
public class Hand {

    private Hand() {
    }
    
    private static Set<Key> leftHand = new HashSet<Key>() {{
            add(Conversion.xkbToScancode.get("TLDE"));
            add(Conversion.xkbToScancode.get("AE01"));
            add(Conversion.xkbToScancode.get("AE02"));
            add(Conversion.xkbToScancode.get("AE03"));
            add(Conversion.xkbToScancode.get("AE04"));
            add(Conversion.xkbToScancode.get("AE05"));
            add(Conversion.xkbToScancode.get("AE06"));

            add(Conversion.xkbToScancode.get("TAB"));
            add(Conversion.xkbToScancode.get("AD01"));
            add(Conversion.xkbToScancode.get("AD02"));
            add(Conversion.xkbToScancode.get("AD03"));
            add(Conversion.xkbToScancode.get("AD04"));
            add(Conversion.xkbToScancode.get("AD05"));

            add(Conversion.xkbToScancode.get("CAPS"));
            add(Conversion.xkbToScancode.get("AC01"));
            add(Conversion.xkbToScancode.get("AC02"));
            add(Conversion.xkbToScancode.get("AC03"));
            add(Conversion.xkbToScancode.get("AC04"));
            add(Conversion.xkbToScancode.get("AC05"));

            add(Conversion.xkbToScancode.get("LFSH"));
            add(Conversion.xkbToScancode.get("LSGT"));
            add(Conversion.xkbToScancode.get("AB01"));
            add(Conversion.xkbToScancode.get("AB02"));
            add(Conversion.xkbToScancode.get("AB03"));
            add(Conversion.xkbToScancode.get("AB04"));
            add(Conversion.xkbToScancode.get("AB05"));

            add(Conversion.xkbToScancode.get("LCTL"));
            add(Conversion.xkbToScancode.get("LWIN"));
            add(Conversion.xkbToScancode.get("LALT"));
            add(Conversion.xkbToScancode.get("SPCE"));
    }};
    private static Set<Key> rightHand = new HashSet<Key>() {{
            add(Conversion.xkbToScancode.get("AE07"));
            add(Conversion.xkbToScancode.get("AE08"));
            add(Conversion.xkbToScancode.get("AE09"));
            add(Conversion.xkbToScancode.get("AE10"));
            add(Conversion.xkbToScancode.get("AE11"));
            add(Conversion.xkbToScancode.get("AE12"));
            add(Conversion.xkbToScancode.get("BKSP"));

            add(Conversion.xkbToScancode.get("AD06"));
            add(Conversion.xkbToScancode.get("AD07"));
            add(Conversion.xkbToScancode.get("AD08"));
            add(Conversion.xkbToScancode.get("AD09"));
            add(Conversion.xkbToScancode.get("AD10"));
            add(Conversion.xkbToScancode.get("AD11"));
            add(Conversion.xkbToScancode.get("AD12"));
            add(Conversion.xkbToScancode.get("RTRN"));

            add(Conversion.xkbToScancode.get("AC06"));
            add(Conversion.xkbToScancode.get("AC07"));
            add(Conversion.xkbToScancode.get("AC08"));
            add(Conversion.xkbToScancode.get("AC09"));
            add(Conversion.xkbToScancode.get("AC10"));
            add(Conversion.xkbToScancode.get("AC11"));
            add(Conversion.xkbToScancode.get("BKSL"));

            add(Conversion.xkbToScancode.get("AB06"));
            add(Conversion.xkbToScancode.get("AB07"));
            add(Conversion.xkbToScancode.get("AB08"));
            add(Conversion.xkbToScancode.get("AB09"));
            add(Conversion.xkbToScancode.get("AB10"));
            add(Conversion.xkbToScancode.get("RTSH"));

            add(Conversion.xkbToScancode.get("SPCE"));
            add(Conversion.xkbToScancode.get("RALT"));
            add(Conversion.xkbToScancode.get("RWIN"));
            add(Conversion.xkbToScancode.get("MENU"));
            add(Conversion.xkbToScancode.get("RCTL"));
    }};
    
    public static boolean isLeftHand(Key key) {
        return leftHand.contains(key);
    }
    public static boolean isRightHand(Key key) {
        return rightHand.contains(key);
    }

}
