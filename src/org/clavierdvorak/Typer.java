/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import java.util.Iterator;

/**
 *
 * @author twilight
 */
public interface Typer {

    public Statistic typeKeySequence(Iterator<Key> keySequence);

}
