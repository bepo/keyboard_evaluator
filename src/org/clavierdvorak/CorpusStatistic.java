/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author twilight
 */
public class CorpusStatistic {
    
    public int totalReadCharacters;
    public Map<Character, Integer> readCharacters = new HashMap<Character, Integer>();
    public Map<Character, Map<Character, Integer>> readDigrams = new HashMap<Character, Map<Character, Integer>>();

    public void enrichMapsWithCharacters(List<Character> chars) {
        for (Character c: chars) {
            readCharacters.put(c, 0);
            readDigrams.put(c, new HashMap<Character, Integer>());
        }
        for (Character c : readCharacters.keySet()) {
            for (Character c2 : readCharacters.keySet()) {
                readDigrams.get(c).put(c2, 0);
            }
        }
    }

    public void initMapsWithCharacters(Collection<Character> chars) {
        for (Character c: chars) {
            readCharacters.put(c, 0);
            readDigrams.put(c, new HashMap<Character, Integer>());
            for (Character c2 : chars) {
                readDigrams.get(c).put(c2, 0);
            }
        }
    }
    
    public Map<Character, Integer> getReadCharacters() {
        return readCharacters;
    }

    public Map<Character, Map<Character, Integer>> getReadDigrams() {
        return readDigrams;
    }

    public int getTotalReadCharacters() {
        return totalReadCharacters;
    }
    
    
}
