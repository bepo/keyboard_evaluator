/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author twilight
 */
public interface KeySelector {

    /* We reuse Iterator interface in a bad way there. We should have a
       dedicated interface for this (TODO cleaning) */
    public Iterator<Key> getKeySequenceFor(Reader characterReader);

    public List<Key> getKeySequenceFor(char character);

    public CorpusStatistic getCorpusStatistic();
    
    /**
     * 
     * @return a map associating to any known key 4 possibly null Characters.
     * Characters are in the following order: simple, shift, altgr, shift+altgr.
     */
    public Map<Key, Character[]> getCharactersForKeys();
}
