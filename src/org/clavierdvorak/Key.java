/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author twilight
 */
public class Key implements Comparable<Key> {

    public static <T> Map<Key, T> indexedMap() {
        // could try identity map here (TODO perf)
        return new HashMap<Key, T>();
    }

    public static <T> Map<Key, T> indexedSortedMap() {
        // could try identity map here (TODO perf)
        return new TreeMap<Key, T>();
    }

    public static Set<Key> set() {
        return new HashSet<Key>();
    }
    
    // pseudo scancode:
    //  - any scancode with two hexadecimal digit is directly encoded
    //  - scancodes looking like E0hh is encoded as 80 | hh
    // examples: 
    //  - 29 -> 29 ; 02 -> 02 ; 3A -> 3A ; ...
    //  - E01D -> 80 | 1D = 9D ; E038 -> C8 ; ...
    // This encoding can be changed if we require 2 digit scancode with higher
    // bit to one (80, 81, ...) or 4 digit scancode with non E0 prefix.
    public byte scancode;

    @Override
    public int hashCode() {
        return scancode;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Key && ((Key)o).scancode == this.scancode;
    }
    
    
    
    public String getScancodeString() {
        if ((scancode & (byte)0x80) == 0) {
            return String.format("%02X", scancode);
        } else {
            return String.format("E0%02X", scancode & ~(byte)0x80);
        }
    }

    public Key(String scancodeString) {
        boolean big = false;
        if (scancodeString.startsWith("E0")) {
            big = true;
            scancodeString = scancodeString.substring(2);
        }
        if (scancodeString.length() != 2) {
            throw new RuntimeException("Unhandled scancode string (wrong length): '"+scancodeString+"'");
        }
        scancode = Byte.parseByte(scancodeString, 16);
        if ((scancode & (byte)0x80) != 0) {
            // in fact parseByte fails first, so this code is useless
            throw new RuntimeException("Unhandled scancode string (too big): '"+scancodeString+"'");
        }
        if (big) {
            scancode |= 0x80;
        }
    }

    public int compareTo(Key other) {
        return this.scancode - other.scancode;
    }

    @Override
    public String toString() {
        return getScancodeString();
    }
}
