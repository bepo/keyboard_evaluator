/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import org.clavierdvorak.keyselector.ClassicKeyboardKeySelector;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author twilight
 */
public class PrinterTools {

    public PrintStream utf8File(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        return new PrintStream(new File(fileName), "utf-8");
    }

    public String keyToString(Key key) {
        return key.getScancodeString() + " [" + Conversion.scancodeToAzerty.get(key) + "]";
    }

    public void keyDigrams(PrintStream out, Map<Key, Map<Key, Integer>> digrams, int totalForRatio, boolean log) {
        int max = max(digrams);
        out.println("<table class='table2d log'>");
        for (Map.Entry<Key, Map<Key, Integer>> e1 : digrams.entrySet()) {
            out.println("<tr>");
            Key k1 = e1.getKey();
            for (Map.Entry<Key, Integer> e2 : e1.getValue().entrySet()) {
                int v = e2.getValue();
                if (v == 0) {
                    out.println("<td/>");
                } else {
                    int col = !log ? 255 - 255 * v / max : (int) (255 - 255 * Math.log(1 + v) / Math.log(1 + max));
                    out.println("<td title='[" + xmlEscape(Conversion.scancodeToAzerty.get(k1)) + "] &#8658; [" + xmlEscape(Conversion.scancodeToAzerty.get(e2.getKey()) + "] : " + (trunc(100. * v / totalForRatio))) + "%' style='background-color: rgb(255, " + col + ", " + col + ")'/>");
                }
            }
            out.println("</tr>");
        }
        out.println("</table>");
    }
    
    public void charDigrams(PrintStream out, Map<Character, Map<Character, Integer>> digrams, int totalForRatio, boolean log) {
        int max = max(digrams);
        out.println("<table class='table2d'>");
        for (Map.Entry<Character, Map<Character, Integer>> e1 : digrams.entrySet()) {
            out.println("<tr>");
            Character k1 = e1.getKey();
            for (Map.Entry<Character, Integer> e2 : e1.getValue().entrySet()) {
                int v = e2.getValue();
                if (v == 0) {
                    out.println("<td/>");
                } else {
                    int col = !log ? 255 - 255 * v / max : (int) (255 - 255 * Math.log(1 + v) / Math.log(1 + max));
                    out.println("<td title='" + xmlEscape(k1.toString()) + " &#8658; " + xmlEscape(e2.getKey().toString() + " : " + trunc(100. * v / totalForRatio)) + "%' style='background-color: rgb(255, " + col + ", " + col + ")'/>");
                }
            }
            out.println("</tr>");
        }
        out.println("</table>");
    }

    public void charFrequences(PrintStream out, final Map<Character, Integer> frequences, int totalForRatio, int mod, String additionalClasses, Main runner) {
        List<Character> orderedCharacters = new ArrayList<Character>(frequences.keySet());
        Collections.sort(orderedCharacters, new Comparator<Character>() {
            public int compare(Character a, Character b) {
                return frequences.get(b) - frequences.get(a);
            }
        });
        out.println("<table class='list "+additionalClasses+"'>");
        out.println("<thead><th>Caractère</th><th>Nb</th><th>%</th><th>Cumul (Nb)</th><th>Cumul (%)</th><th>Séquence Utilisée</th></thead>");
        int bg = 0;
        int cumul = 0;
        for (Character c : orderedCharacters) {
            int v = frequences.get(c);
            if (v == 0) {
                break;
            }
            cumul += v;
            String howTyped = "";
            for (Key key : runner.keySelector.getKeySequenceFor(c)) {
                if (!"".equals(howTyped)) {
                    howTyped += " + ";
                }
                howTyped += "["+Conversion.scancodeToAzerty.get(key)+"]";
            }
            out.println("<tr class='listLine"+bg+"'><td>"+xmlEscape(c)+"</td><td>"+v+"</td><td>"+(100.*v/totalForRatio)+"</td><td>"+cumul+"</td><td>"+(100.*cumul/totalForRatio)+"</td><td>"+howTyped+"</td></tr>");
            bg = (bg + 1) % mod;
        }
        out.println("</table>");
    }

    public void keyFrequences(PrintStream out, final Map<Key, Integer> frequences, int totalForRatio, int mod, String additionalClasses) {
        List<Key> orderedKeys = new ArrayList<Key>(frequences.keySet());
        Collections.sort(orderedKeys, new Comparator<Key>() {
            public int compare(Key a, Key b) {
                return frequences.get(b) - frequences.get(a);
            }
        });
        out.println("<table class='list "+additionalClasses+"'>");
        out.println("<thead><th>Scancode</th><th>Xkb</th><th>Azerty</th><th>Nb</th><th>%</th></thead>");
        int bg = 0;
        for (Key c : orderedKeys) {
            int v = frequences.get(c);
            out.println("<tr class='listLine" + bg + "'><td>" + xmlEscape(c.getScancodeString()) + "</td><td>" + xmlEscape(Conversion.scancodeToXkb.get(c)) + "</td><td>[" + xmlEscape(Conversion.scancodeToAzerty.get(c)) + "]</td><td>" + v + "</td><td>" + (100. * v / totalForRatio) + "</td></tr>");
            bg = (bg + 1) % mod;
        }
        out.println("</table>");
    }

    public void row(PrintStream out, Object ...args) {
        out.println("<tr>");
        for (Object object : args) {
            out.println("<td>"+xmlEscape(object.toString())+"</td>");
        }
        out.println("</tr>");
    }

    enum KeyShape {

        normal(16),
        backspace(32),
        tab(25),
        ret(23),
        capslock(30),
        lshift(22),
        rshift(45),
        free(-1);
        double width;
        private KeyShape(double w) {
            width = w;
        }
    }

    public void svgKeyboard(PrintStream out, ClassicKeyboardKeySelector keySelector) {
        Map<Key, Character[]> characters = keySelector.getCharactersForKeys();
        List<Object[]> svgKeys = new ArrayList<Object[]>();
        svgKeys.add(new Object[]{"29", KeyShape.normal});
        svgKeys.add(new Object[]{"02", KeyShape.normal});
        svgKeys.add(new Object[]{"03", KeyShape.normal});
        svgKeys.add(new Object[]{"04", KeyShape.normal});
        svgKeys.add(new Object[]{"05", KeyShape.normal});
        svgKeys.add(new Object[]{"06", KeyShape.normal});
        svgKeys.add(new Object[]{"07", KeyShape.normal});
        svgKeys.add(new Object[]{"08", KeyShape.normal});
        svgKeys.add(new Object[]{"09", KeyShape.normal});
        svgKeys.add(new Object[]{"0A", KeyShape.normal});
        svgKeys.add(new Object[]{"0B", KeyShape.normal});
        svgKeys.add(new Object[]{"0C", KeyShape.normal});
        svgKeys.add(new Object[]{"0D", KeyShape.normal});
        svgKeys.add(new Object[]{"0E", KeyShape.backspace});
        svgKeys.add(null);
        svgKeys.add(new Object[]{"0F", KeyShape.tab});
        svgKeys.add(new Object[]{"10", KeyShape.normal});
        svgKeys.add(new Object[]{"11", KeyShape.normal});
        svgKeys.add(new Object[]{"12", KeyShape.normal});
        svgKeys.add(new Object[]{"13", KeyShape.normal});
        svgKeys.add(new Object[]{"14", KeyShape.normal});
        svgKeys.add(new Object[]{"15", KeyShape.normal});
        svgKeys.add(new Object[]{"16", KeyShape.normal});
        svgKeys.add(new Object[]{"17", KeyShape.normal});
        svgKeys.add(new Object[]{"18", KeyShape.normal});
        svgKeys.add(new Object[]{"19", KeyShape.normal});
        svgKeys.add(new Object[]{"1A", KeyShape.normal});
        svgKeys.add(new Object[]{"1B", KeyShape.normal});
        svgKeys.add(new Object[]{"1C", KeyShape.ret});
        svgKeys.add(null);
        svgKeys.add(new Object[]{"3A", KeyShape.capslock});
        svgKeys.add(new Object[]{"1E", KeyShape.normal});
        svgKeys.add(new Object[]{"1F", KeyShape.normal});
        svgKeys.add(new Object[]{"20", KeyShape.normal});
        svgKeys.add(new Object[]{"21", KeyShape.normal});
        svgKeys.add(new Object[]{"22", KeyShape.normal});
        svgKeys.add(new Object[]{"23", KeyShape.normal});
        svgKeys.add(new Object[]{"24", KeyShape.normal});
        svgKeys.add(new Object[]{"25", KeyShape.normal});
        svgKeys.add(new Object[]{"26", KeyShape.normal});
        svgKeys.add(new Object[]{"27", KeyShape.normal});
        svgKeys.add(new Object[]{"28", KeyShape.normal});
        svgKeys.add(new Object[]{"2B", KeyShape.normal});
        svgKeys.add(null);
        svgKeys.add(new Object[]{"2A", KeyShape.lshift});
        svgKeys.add(new Object[]{"56", KeyShape.normal});
        svgKeys.add(new Object[]{"2C", KeyShape.normal});
        svgKeys.add(new Object[]{"2D", KeyShape.normal});
        svgKeys.add(new Object[]{"2E", KeyShape.normal});
        svgKeys.add(new Object[]{"2F", KeyShape.normal});
        svgKeys.add(new Object[]{"30", KeyShape.normal});
        svgKeys.add(new Object[]{"31", KeyShape.normal});
        svgKeys.add(new Object[]{"32", KeyShape.normal});
        svgKeys.add(new Object[]{"33", KeyShape.normal});
        svgKeys.add(new Object[]{"34", KeyShape.normal});
        svgKeys.add(new Object[]{"35", KeyShape.normal});
        svgKeys.add(new Object[]{"36", KeyShape.rshift});
        svgKeys.add(null);
        svgKeys.add(new Object[]{"1D",   KeyShape.free, 25});
        svgKeys.add(new Object[]{"E01F", KeyShape.free, 25});
        svgKeys.add(new Object[]{"38",   KeyShape.free, 25});
        svgKeys.add(new Object[]{"39",   KeyShape.free, 92});
        svgKeys.add(new Object[]{"E038", KeyShape.free, 25});
        svgKeys.add(new Object[]{"E027", KeyShape.free, 20});
        svgKeys.add(new Object[]{"E02F", KeyShape.free, 20});
        svgKeys.add(new Object[]{"E01D", KeyShape.free, 26});
        double factor = 3;
        double h = 16;
        double inter = 3;
        double totalWidth = (13 * (inter+KeyShape.normal.width) +KeyShape.backspace.width) * factor;
        double totalHeight = (5 * (inter + h)) * factor;
        out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
        out.println("<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='"+totalWidth+"' height='"+totalHeight+"'>");
        out.println("  <defs");
        out.println("     id='defs2764'>");
        out.println("    <filter");
        out.println("       id='filter4067'");
        out.println("       x='-0.12'");
        out.println("       width='1.24'");
        out.println("       y='-0.12'");
        out.println("       height='1.24'>");
        out.println("      <feGaussianBlur");
        out.println("         stdDeviation='2.746063'");
        out.println("         id='feGaussianBlur4069' />");
        out.println("    </filter>");
        out.println("  </defs>n");
        double x = 0;
        double y = 0;
        for (Object[] description : svgKeys) {
            if (description == null) {
                x = 0;
                y += h + inter;
            } else {
                KeyShape shape = (KeyShape) description[1];
                double w = shape.width;
                if (shape == KeyShape.free) {
                    w = ((Number) description[2]).doubleValue();
                }
                Character[] keyCharacters = characters.get(new Key((String) description[0]));
                if (keyCharacters == null) {
                    keyCharacters = new Character[]{null, null, null, null};
                }
                if (shape == KeyShape.ret) {
                    returnShape(out, "key" + description[0], inter,
                            x * factor, y * factor, w * factor, h * factor, 3 * factor,
                            xmlEscape(keyCharacters[0]), xmlEscape(keyCharacters[1]),
                            xmlEscape(keyCharacters[2]), xmlEscape(keyCharacters[3]));
                } else {
                    rectangleShape(out, "key" + description[0],
                            x * factor, y * factor, w * factor, h * factor, 3 * factor,
                            xmlEscape(keyCharacters[0]), xmlEscape(keyCharacters[1]),
                            xmlEscape(keyCharacters[2]), xmlEscape(keyCharacters[3]));
                }
                x += w + inter;
            }
        }
        out.println("</svg>");
    }

    private void returnShape(PrintStream out, String id, double inter, double x, double y, double w, double h, double r, String normal, String shift, String altgr, String altgrshift) {
        out.println("    <g");
        out.println("       id='" + id + "'");
        out.println("       transform='translate(" + x + ", " + y + ")'>");
        out.println("      <path");
        out.println("         style='font-size:20.15510178px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-tb;text-anchor:start;fill:#f7f7f7;fill-opacity:1;stroke:#000000;stroke-width:2;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;display:inline;font-family:Linux Libertine O'");
        out.println("         id='path__"+id+"'");
        out.println("         d='M 1 1 L "+(w-1)+" 1 L "+(w-1)+" "+(2*h+inter-1)+" L "+((int)(w/3) -1)+" "+(2*h+inter-1)+" L "+((int)(w/3)-1)+" "+(h-1)+" L 1 "+(h-1)+" z' />");
        out.println("    </g>");
    }
    private void rectangleShape(PrintStream out, String id, double x, double y, double w, double h, double r, String normal, String shift, String altgr, String altgrshift) {
        out.println("    <g");
        out.println("       id='" + id + "'");
        out.println("       transform='translate(" + x + ", " + y + ")'>");
        out.println("      <rect");
        out.println("         ry='"+r+"'");
        out.println("         style='font-size:20.15510178px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-tb;text-anchor:start;fill:#f7f7f7;fill-opacity:1;stroke:#000000;stroke-width:2;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;display:inline;font-family:Linux Libertine O'");
        out.println("         id='rect2__"+id+"'");
        out.println("         width='"+(w-2)+"'");
        out.println("         height='"+(h-2)+"'");
        out.println("         x='1'");
        out.println("         y='1'");
        out.println("         rx='"+r+"' />");
        if (normal != null) {
            out.println("      <text");
            out.println("         id='text1__" + id + "'");
            out.println("         x='" + (.15 * w) + "'");
            out.println("         y='" + (.80 * h) + "'");
            out.println("         style='font-size:" + (0.35 * h) + "px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-tb;text-anchor:start;fill:#000000;fill-opacity:1;stroke:#000000;stroke-width:1;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;display:inline;font-family:Linux Libertine O'");
            out.println("         xml:space='preserve'><tspan>" + normal + "</tspan></text>");
        }
        if (shift != null) {
            out.println("      <text");
            out.println("         id='text2__" + id + "'");
            out.println("         x='" + (.15 * w) + "'");
            out.println("         y='" + (.42 * h) + "'");
            out.println("         style='font-size:" + (0.35 * h) + "px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-tb;text-anchor:start;fill:#ff8181;fill-opacity:1;stroke:none;stroke-width:2.25;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;display:inline;font-family:Linux Libertine O'");
            out.println("         xml:space='preserve'><tspan>" + shift + "</tspan></text>");
        }
        if (altgr != null) {
            out.println("      <text");
            out.println("         id='text3__" + id + "'");
            out.println("         x='" + (.85 * w) + "'");
            out.println("         y='" + (.80 * h) + "'");
            out.println("         style='font-size:" + (0.35 * h) + "px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-tb;text-anchor:start;fill:#819fff;fill-opacity:1;stroke:none;stroke-width:2.25;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;display:inline;font-family:Linux Libertine O'");
            out.println("         xml:space='preserve'><tspan style='text-align:end;line-height:100%;writing-mode:lr-tb;text-anchor:end'>" + altgr + "</tspan></text>");
        }
        if (altgrshift != null) {
            out.println("      <text");
            out.println("         id='text4__" + id + "'");
            out.println("         x='" + (.85 * w) + "'");
            out.println("         y='" + (.42 * h) + "'");
            out.println("         style='font-size:" + (0.35 * h) + "px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:start;line-height:125%;writing-mode:lr-tb;text-anchor:start;fill:#eaa6ff;fill-opacity:1;stroke:none;stroke-width:2.25;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;display:inline;font-family:Linux Libertine O'");
            out.println("         xml:space='preserve'><tspan style='text-align:end;line-height:100%;writing-mode:lr-tb;text-anchor:end'>" + altgrshift + "</tspan></text>");
        }
        out.println("    </g>");
    }


    private <T>int max(Map<T, Map<T, Integer>> digrams) {
        int max = -1;
        for (Map.Entry<T, Map<T, Integer>> e1 : digrams.entrySet()) {
            Object k1 = e1.getKey();
            for (Map.Entry<T, Integer> e2 : e1.getValue().entrySet()) {
                if (e2.getValue()>max) {
                    max = e2.getValue();
                }
            }
        }
        return max;
    }

    private String trunc(double d) {
        String res = ""+d;
        return res.length() <= 6 ? res : res.substring(0, 6);
    }

    private String xmlEscape(String text) {
        return text.replace("&", "&#38;").replace("<", "&#60;").replace("'", "&#39;").replace("\"", "&#34;");
    }

    private String xmlEscape(Character character) {
        if (character == null) {
            return null;
        } else {
            return character.toString().replace("&", "&#38;").replace("<", "&#60;").replace("'", "&#39;").replace("\"", "&#34;");
        }
    }
}
