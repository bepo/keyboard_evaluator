/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import org.clavierdvorak.klaylike.KlayTyper;
import org.clavierdvorak.keyselector.ClassicKeyboardKeySelector;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 *
 * @author twilight
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        if (args.length == 1) {
            if (args[0].equals("UI")) {
                org.clavierdvorak.ui.Main.main(args);
                return;
            }
            if (args[0].equals("TEST")) {
                org.clavierdvorak.costtable.Main.main(args);
                return;
            }
        }
        String javascriptConfigurationFileName = "start.js";
        String javascriptPrinterFileName = "print.js";
        
        // Read command line parameters
        Map<String,String> configuration = new LinkedHashMap<String, String>();
        String key = null;
        for (String arg : args) {
            if (key == null) {
                if (arg.startsWith("-")) {
                    key = arg;
                } else {
                    System.out.println("Ignored parameter: "+arg);
                }
            } else {
                if (arg.startsWith("-")) {
                    configuration.put(key.substring(1), null);
                    key = arg;
                } else {
                    configuration.put(key.substring(1), arg);
                    key = null;
                }
            }
        }
        if (key != null) {
            configuration.put(key.substring(1), null);
        }


        Main runner = new Main();
        
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");
        jsEngine.put("args", configuration);
        jsEngine.put("runner", runner);
        jsEngine.eval(new InputStreamReader(new FileInputStream(javascriptConfigurationFileName), "utf-8"));
        
        runner.evaluate();
        runner.corpusStatistic = runner.keySelector.getCorpusStatistic();
        jsEngine.put("corpusStat", runner.corpusStatistic);
        jsEngine.put("stat", runner.statistic);
        jsEngine.put("tools", new PrinterTools());
        jsEngine.eval(new InputStreamReader(new FileInputStream(javascriptPrinterFileName), "utf-8"));
        
        /*
        String corpusEncoding = "ISO-8859-1";
        String corpusFileName = "benchmark.txt";
        String layoutFileName = "layout_63.xkb";
        KeySequenceSelector keySelector = new ClassicKeyboardKeySelector(layoutFileName);
        Typer typer = null;
        TyperStatisticPrinter printer = null;
        
        Reader characterReader = new InputStreamReader(new FileInputStream(corpusFileName), corpusEncoding);
        TyperStatistic statistic = typer.typeKeySequence(keySelector.getKeySequenceFor(characterReader));
        printer.printStatistic(statistic);
        */
    }

    public String corpus = null;
    public String corpusEncoding = "utf-8";
    
    private CorpusStatistic corpusStatistic;
    private Statistic statistic;
    private Typer typer = null;
    public KeySelector keySelector = null;
    
    public void chooseMinimalKeyStrokesFromLayout(String layoutFileName) throws FileNotFoundException, IOException {
        keySelector = new ClassicKeyboardKeySelector(layoutFileName);
    }
    
    public void typerFromKlay() {
        typer = new KlayTyper();
    }
    
    public void evaluate() throws FileNotFoundException, UnsupportedEncodingException {
        Reader characterReader = new BufferedReader(new InputStreamReader(new FileInputStream(corpus), corpusEncoding));
        Iterator<Key> keySequence = keySelector.getKeySequenceFor(characterReader);
        statistic = typer.typeKeySequence(keySequence);
    }
    
}
