/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.keyselector;

import org.clavierdvorak.*;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author twilight
 */
public class EmptyKeySelector implements KeySelector {

    private CorpusStatistic corpusStatistic = new CorpusStatistic();
        
    public Iterator<Key> getKeySequenceFor(Reader characterReader) {
        try {
            char[] buf = new char[1];
            Character lastRead = null;
            while (true) {
                if (-1 == characterReader.read(buf)) {
                    throw new IOException();
                }
                Character read = buf[0];
                corpusStatistic.totalReadCharacters++;
                add(1, corpusStatistic.readCharacters, read, corpusStatistic);
                if (lastRead != null) {
                    add(1, corpusStatistic.readDigrams.get(lastRead), read, corpusStatistic);
                }
                lastRead = read;
            }
        } catch (IOException ex) {
            try {
                characterReader.close();
            } catch (Exception e) {
            }
        }

        return Collections.<Key>emptyList().iterator();
    }

    private void add(int incr, Map<Character, Integer> map, Character ch, CorpusStatistic corpusStatistic) {
        if (!map.containsKey(ch)) {
            corpusStatistic.enrichMapsWithCharacters(Collections.singletonList(ch));
            map.put(ch, incr);
        } else {
            map.put(ch, map.get(ch) + incr);
        }
    }

    public List<Key> getKeySequenceFor(char character) {
        return Collections.emptyList();
    }

    public CorpusStatistic getCorpusStatistic() {
        return corpusStatistic;
    }

    public Map<Key, Character[]> getCharactersForKeys() {
        return Collections.emptyMap();
    }

}
