/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.keyselector;

import java.io.IOException;

/**
 *
 * @author twilight
 */
public class KeySequenceEndedException extends RuntimeException {
    // runtime as we reuse iterator ...

    public KeySequenceEndedException(String message, IOException  cause) {
        super(message, cause);
    }

}
