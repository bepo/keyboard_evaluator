/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.keyselector;

import org.clavierdvorak.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
// Java6 feature
//import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author twilight
 */
public class ClassicKeyboardKeySelector implements KeySelector {

    // We could use some dedicated efficient collections there (TODO perf)
    // We could try and play with the load factor
    private Map<Character, List<Key>> conversion = new HashMap<Character, List<Key>>();
    private Map<Key, Character[]> keyCharacters = new HashMap<Key, Character[]>();
    private CorpusStatistic corpusStatistic;

    public Map<Key, Character[]> getCharactersForKeys() {
        return Collections.unmodifiableMap(keyCharacters);
    }
    
    public CorpusStatistic getCorpusStatistic() {
        return corpusStatistic;
    }

    public void setCorpusStatistic(CorpusStatistic corpusStatistic) {
        this.corpusStatistic = corpusStatistic;
    }
    
    public ClassicKeyboardKeySelector(String layoutFileName) throws FileNotFoundException, IOException {
        this(new FileInputStream(layoutFileName));
    }
    public ClassicKeyboardKeySelector(InputStream layout) throws IOException {
        // example lines: key <AE05> { [          parenright,            5,   bracketright,rightdoublequotemark ] }; 
        //                key <AE12> { [             percent,           mu,          U2030                      ] }; 
        BufferedReader reader = new BufferedReader(new InputStreamReader(layout));
        String line;
        Pattern topLevelPattern = Pattern.compile(".*<([A-Z0-9]{4})>.*\\[([^]]*)\\].*");
        Key altGr = Conversion.xkbToScancode.get("RALT");
        Key leftShift = Conversion.xkbToScancode.get("LFSH");
        Key rightShift = Conversion.xkbToScancode.get("RTSH");
        List<Key> listForNewLine = Collections.emptyList();
        conversion.put('\n', listForNewLine);
        conversion.put('\r', listForNewLine);
        while ((line = reader.readLine()) != null) {
            // could handle comments, ... TODO usability
            //System.out.println("READING "+line);
            if (line.matches("^\\s*//.*")) {
                continue;
            }
            Matcher matcher = topLevelPattern.matcher(line);
            if (matcher.matches()) {
                Key key = Conversion.xkbToScancode.get(matcher.group(1));
                if (key==null) {
                    System.out.println("Problem with xkb: no scancode found for xkb key '"+matcher.group(1)+"' in our map ... skipping this key");
                    continue;
                }
                String symbolsString = matcher.group(2);
                String[] symbols = symbolsString.split(",");
                for (int i = 0; i < symbols.length; i++) {
                    symbols[i] = symbols[i].replaceAll("[ \\t]", "");
                }
                Character[] chars = new Character[4];
                keyCharacters.put(key, chars);
                //System.out.println("ADDING " + symbols[0] + " -> " + key.getScancodeString());
                if (symbols.length > 0) {
                    String symbol = symbols[0];
                    try {
                    char character = Conversion.xkbToCharacter.get(symbol);
                    chars[0] = character;
                    if (!conversion.containsKey(character)) conversion.put(character, Collections.singletonList(key));
                    if (symbols.length > 1) {
                        symbol = symbols[1];
                        character = Conversion.xkbToCharacter.get(symbol);
                        chars[1] = character;
                        Key shift = Hand.isRightHand(key) ? leftShift : rightShift;
                        if (!conversion.containsKey(character)) conversion.put(character, list(shift, key));
                        if (symbols.length > 2) {
                            symbol = symbols[2];
                            character = Conversion.xkbToCharacter.get(symbol);
                            chars[2] = character;
                            if (!conversion.containsKey(character)) conversion.put(character, list(altGr, key));
                            if (symbols.length > 3) {
                                symbol = symbols[3];
                                character = Conversion.xkbToCharacter.get(symbol);
                                chars[3] = character;
                                if (!conversion.containsKey(character)) conversion.put(character, list(altGr, shift, key));
                            }
                        }
                    }
                    } catch (NullPointerException e) {
                        System.out.println("Problem with xkb: probably xkb symbol '"+symbol+"' is not found in our map");
                    }
                }
            }
        }
        addDeadsIfNotPresents("dead_circumflex", "^âêîôûŷÂÊÎÔÛŶ");
        addDeadsIfNotPresents("dead_diaeresis",  " äëïöüÿÄËÏÖÜŸ");
        addDeadsIfNotPresents("dead_acute",      " áéíóúýÁÉÍÓÚÝ");
        addDeadsIfNotPresents("dead_grave",      "`àèìòùỳÀÈÌÒÙỲ");
        addDeadsIfNotPresents("dead_tilde",      "~ã ĩõũ Ã ĨÕŨ ");
        addDeadsIfNotPresents("dead_abovering",  "°å   ů Å   Ů ");
        corpusStatistic = new CorpusStatistic();
        corpusStatistic.initMapsWithCharacters(conversion.keySet());
    }

    private void addDeadsIfNotPresents(String deadkey, String characters) {
        String[] mapTo = {"space", "a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y"};
        if (characters.length() != mapTo.length) {
            System.out.println("Problem: wrong addDeadsIfNotPresents '"+characters+"' for dead key '"+deadkey+"'");
        }
        for (int i = 0; i < mapTo.length; i++) {
            addIfNotPresent(characters.charAt(i), deadkey, mapTo[i]);
        }
    }
    
    private void addIfNotPresent(char c, String ... xkbNames) {
        if (!conversion.containsKey(c)) {
            ArrayList<Key> list = new ArrayList<Key>();
            for (String xkb : xkbNames) {
                List<Key> strokes = conversion.get(Conversion.xkbToCharacter.get(xkb));
                if (strokes != null) {
                    list.addAll(strokes);
                } else {
                    System.out.println("Problem: no way to type '"+Conversion.xkbToCharacter.get(xkb)+"' (xkb '"+xkb+"') has been found to type '"+c+"' ... this character won't be typable");
                }
            }
            conversion.put(c, list);
        }
    }
    
    private void add(int incr, Map<Character, Integer> map, Character ch) {
        map.put(ch, map.get(ch) + incr);
    }
    
    private <T> List<T> list(T ... objects) {
        return Arrays.asList(objects);
    }
    
    public boolean queueKeysForCharacter(Queue<Key> queue, Character c) {
        List<Key> keys = conversion.get(c);
        if (keys == null) {
            System.out.println("Problem: no way to type character '"+c+"' (decimal utf-8 '"+(int)c+"') (xkb '"+Conversion.characterToXkb.get(c)+"') has been found ... skipped");
            return false;
        } else {
            for (Key k : keys) {
                queue.add(k);
            }
            return true;
        }
    }

    public List<Key> getKeySequenceFor(final char character) {
        return Collections.unmodifiableList(conversion.get(character));
    }

    public Iterator<Key> getKeySequenceFor(final Reader characterReader) {
        return new Iterator<Key>() {

            public boolean hasNext() {
                return true;
            }

            //Java6 feature
            //Queue<Key> keyQueue = new ArrayDeque<Key>();
            Queue<Key> keyQueue = new LinkedList<Key>();
            Character lastRead = null;
            public Key next() {
                if (!keyQueue.isEmpty()) {
                    return keyQueue.poll();
                }
                try {
                    char[] buf = new char[1];
                    while (keyQueue.isEmpty()) {
                        if (-1 == characterReader.read(buf)) {
                            throw new IOException();
                        }
                        Character read = buf[0];
                        if (queueKeysForCharacter(keyQueue, read)) {
                            corpusStatistic.totalReadCharacters++;
                            add(1, corpusStatistic.readCharacters, read);
                            if (lastRead != null) {
                                add(1, corpusStatistic.readDigrams.get(lastRead), read);
                            }
                            lastRead = read;
                        }
                    }
                    return keyQueue.poll();
                } catch (IOException ex) {
                    try {
                        characterReader.close();
                    } catch (Exception e) {}
                    throw new KeySequenceEndedException("Key Sequence Ended", ex);
                }
            }

            public void remove() {
                throw new UnsupportedOperationException("Removing is not supported.");
            }

        };
    }

}
