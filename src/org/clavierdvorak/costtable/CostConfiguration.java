/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.clavierdvorak.Conversion;
import org.clavierdvorak.Key;

/**
 *
 * @author twilight
 */
public class CostConfiguration {
    
    public static final class KeyInformation {
        public final double cost;
        public final int row;
        public final int column;
        public final Finger finger;

        public KeyInformation(double cost, int row, int column, Finger finger) {
            this.cost = cost;
            this.row = row;
            this.column = column;
            this.finger = finger;
        }

        @Override
        public String toString() {
            return "KeyInformation("+cost+", "+row+", "+column+", "+finger.toString()+")";
        }
    }
    
    public static final class FingerInformation {
        public final double length;

        public FingerInformation(double length) {
            this.length = length;
        }
        
        @Override
        public String toString() {
            return "FingerInformation("+length+")";
        }
    }

    final Key spaceKey = Conversion.xkbToScancode.get("SPCE");
    private String name;
    List<String> rules = new ArrayList<String>();
    Map<Key, KeyInformation> leftHandKeys = Key.indexedSortedMap();
    Map<Key, KeyInformation> rightHandKeys = Key.indexedSortedMap();
    KeyInformation leftSpace = null;
    KeyInformation rightSpace = null;
    double[] fadeFactors = {1, 0};

    public Map<Key, KeyInformation> getLeftHandKeys() {
        return leftHandKeys;
    }

    public Map<Key, KeyInformation> getRightHandKeys() {
        return rightHandKeys;
    }

    public String getName() {
        return name;
    }

    public List<String> getRules() {
        return rules;
    }
    
    public CostConfiguration(InputStream in) throws IOException {
        this(new BufferedReader(new InputStreamReader(in)));
    }
    public CostConfiguration(BufferedReader in) throws IOException {
        String line;
        boolean inRule = false;
        String rule = null;
        while ((line = in.readLine()) != null) {
            if (line.startsWith("//")) continue;
            if (line.matches("^\\s*$")) continue;
            if ("..".equals(line.trim())) {
                if (!inRule) {
                    msg("Rule closing found while not in a rule ... ignored closing");
                    continue;
                }
                inRule = false;
                rules.add(rule);
                msg(rule);
                rule = null;
                continue;
            }
            if (inRule) {
                rule += line + "\n";
                continue;
            }
            String[] parts = line.split(" +");
            int p = 0;
            if (Conversion.xkbToScancode.containsKey(parts[0])) {
                processKeyLine(parts);
                continue;
            }
            try {
                Finger.valueOf(parts[0]);
                processFingerLine(parts);
                continue;
            } catch (IllegalArgumentException e) {
                // First element is not a finger name
            }
            if ("....".equals(line.trim())) {
                if (inRule) {
                    msg("Rule opening found while already in a rule (missing close?) ... ignored opening (rules merged)");
                    continue;
                }
                inRule = true;
                rule = "";
                continue;
            }
            if ("FADE".equals(parts[0])) {
                processFadeLine(parts);
                continue;
            }
            msg("First element unrecognized ("+parts[0]+") ... ignored line");
        }
        if (leftSpace == null || rightSpace == null) {
            msg("A space (SPCE) key must be defined for both hands");
            throw new RuntimeException("A space (SPCE) key must be defined for both hands");
        } else if (leftSpace.cost != rightSpace.cost) {
            msg("Different costs for left ("+leftSpace.cost+") and right "+rightSpace.cost+" space keys ... using "+leftSpace.cost);
            rightSpace = new KeyInformation(leftSpace.cost, rightSpace.row, rightSpace.column, rightSpace.finger);
        }
    }

    public Finger keyToFinger(Key key) {
        KeyInformation info = leftHandKeys.get(key);
        if (info == null) {
            return rightHandKeys.get(key).finger;
        } else {
            return info.finger;
        }
    }

    private void msg(Object msg) {
        System.err.println("KeysConfiguration: "+msg);
    }

    private boolean processFadeLine(String[] parts) {
        if (parts.length < 2) {
            msg("FADE must specify at least one value");
            return true;
        }
        double[] newFade = new double[parts.length+1-1];
        newFade[0] = 1.;
        for (int i = 1; i < parts.length; i++) {
            try {
                newFade[i] = Double.parseDouble(parts[i]);
            } catch (NumberFormatException ex) {
                msg("Expected reals on @FADE line, got '"+parts[i]+"' ... ignoring @FADE line");
                return true;
            }
        }
        fadeFactors = newFade;
        return false;
    }

    private boolean processFingerLine(String[] parts) {
        if (parts.length != 2) {
            msg("Wrong parts count (" + parts.length + ") ... ignored line");
            return true;
        }
        return false;
    }

    private boolean processKeyLine(String[] parts) {
        if (parts.length != 5) {
            msg("Wrong parts count (" + parts.length + ") ... ignored line");
            return true;
        }
        int p;
        Finger finger;
        p = 4;
        try {
            finger = Finger.valueOf(parts[p]);
        } catch (IllegalArgumentException e) {
            msg("Wrong finger '" + parts[p] + "' given ... ignored line");
            return true;
        }
        p = 0;
        Key key = Conversion.xkbToScancode.get(parts[p]);
        if ((finger.isIsRightHand() ? rightHandKeys : leftHandKeys).containsKey(key)) {
            msg("Key already defined in this hand (" + parts[p] + ") ... ignored line");
            return true;
        }
        double cost;
        p++;
        try {
            cost = Double.parseDouble(parts[p].replaceFirst("^\\+", ""));
        } catch (NumberFormatException e) {
            msg("Wrong cost '" + parts[p] + "' given ... ignored line");
            return true;
        }
        int row;
        p++;
        try {
            row = Integer.parseInt(parts[p].replaceFirst("^\\+", ""));
        } catch (NumberFormatException e) {
            msg("Wrong row '" + parts[p] + "' given ... ignored line");
            return true;
        }
        int column;
        p++;
        try {
            column = Integer.parseInt(parts[p].replaceFirst("^\\+", ""));
        } catch (NumberFormatException e) {
            msg("Wrong column '" + parts[p] + "' given ... ignored line");
            return true;
        }
        final KeyInformation info = new KeyInformation(cost, row, column, finger);
        if (key.equals(spaceKey)) {
            if (finger.isIsRightHand()) {
                if (rightSpace != null) {
                    msg("Right space already defined ... ignored line");
                    return true;
                }
                rightSpace = info;
            } else {
                if (leftSpace != null) {
                    msg("Left space already defined ... ignored line");
                    return true;
                }
                leftSpace = info;
            }
        } else {
            (finger.isIsRightHand() ? rightHandKeys : leftHandKeys).put(key, info);
        }
        //msg(" --- "+keys.get(key));
        return false;
    }
    
}
