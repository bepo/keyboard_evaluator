/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

/**
 *
 * @author twilight
 */
public interface Function<P1, P2, R> {
    
    public R eval(P1 first, P2 second);

}
