/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import org.clavierdvorak.Conversion;
import org.clavierdvorak.Key;
import org.clavierdvorak.Statistic;
import org.clavierdvorak.Typer;
import org.clavierdvorak.keyselector.KeySequenceEndedException;

/**
 *
 * @author twilight
 */
public class DikeyTableTyper implements Typer {
    
    private Map<Key, Map<Key, Double>> dikeyCosts = Key.indexedMap();
    private Map<Key, Double> unikeyCosts = Key.indexedMap();
    private Map<Key, Finger> keyToFinger = null;
    private double[] fadeFactors = null;
    
    void setUnikeyCost(Key key, double cost) {
        unikeyCosts.put(key, cost);
    }

    void setKeyToFinger(Map<Key, Finger> keyToFinger) {
        this.keyToFinger = keyToFinger;
    }

    void setFadeFactors(double[] fadeFactors) {
        this.fadeFactors = fadeFactors;
    }
    
    public void initAtValue(Collection<Key> keys, double value) {
        for (Key key : keys) {
            Map<Key, Double> part = dikeyCosts.get(key);
            if (part == null) {
                part = Key.indexedMap();
                dikeyCosts.put(key, part);
            }
            for (Key secondKey : keys) {
                part.put(secondKey, value);
            }
        }
    }

    public void initAtValue(Collection<Key> keys, Function<Key, Key, Double> initializer) {
        for (Key key : keys) {
            Map<Key, Double> part = dikeyCosts.get(key);
            if (part == null) { // necessary for example with "space" that is added twice
                part = Key.indexedMap();
                dikeyCosts.put(key, part);
            }
            for (Key secondKey : keys) {
                part.put(secondKey, initializer.eval(key, secondKey));
            }
        }
    }

    public Statistic typeKeySequence(Iterator<Key> keySequence) {
        final Key space = Conversion.xkbToScancode.get("SPCE");
        DikeyTableStatistic stat = new DikeyTableStatistic();
        try {
            Key previousKey = null;
            Finger previousFinger = null;
            Key previousLeft = null;
            int previousLeftDelay = 0;
            Key previousRight = null;
            int previousRightDelay = 0;
            Key key;
            Finger finger;
            key = keySequence.next();
            finger = keyToFinger.get(key); // in case of space choice is not of major importance
            while (true) {
                { // "commit" previous key typing
                    stat.pressedKeysCount++;
                    stat.perFingerPressedKeysCount[finger.getUniqueIndex()]++;
                    if (finger.isIsRightHand()) {
                        previousRight = key;
                        previousRightDelay = 0;
                        previousLeftDelay++;
                    } else {
                        previousLeft = key;
                        previousLeftDelay = 0;
                        previousRightDelay++;
                    }
                    previousKey = key;
                    previousFinger = finger;
                }
                key = keySequence.next(); // break can happen here
                if (space.equals(key)) {
                    finger = previousFinger.isIsRightHand() ? Finger.LeftThumb : Finger.RightThumb;
                } else {
                    finger = keyToFinger.get(key);
                }
                {
                    final Double cost = unikeyCosts.get(key);
                    stat.unikeyCost += cost;
                    stat.difingersCount[previousFinger.getUniqueIndex()][finger.getUniqueIndex()]++;
                    if (previousFinger.isIsRightHand() != finger.isIsRightHand()) {
                        stat.alternanceCount++;
                    }
                    if (finger.isIsRightHand()) {
                        if (previousRight != null) {
                            double fade = getFadeFactor(previousRightDelay);
                            stat.rightHandSeparateCost += fade * dikeyCosts.get(previousRight).get(key);
                        }
                    } else {
                        if (previousLeft != null) {
                            double fade = getFadeFactor(previousLeftDelay);
                            stat.leftHandSeparateCost += fade * dikeyCosts.get(previousLeft).get(key);
                        }
                    }
                }
            }
        } catch (KeySequenceEndedException e) {
            // end of sequence
        }
        return stat;
    }

    /**
     * Returns internal table of costs for visualisation purpose.
     * @return
     */
    Map<Key, Map<Key, Double>> getCosts() {
        return Collections.unmodifiableMap(dikeyCosts);
    }

    Map<Key, Finger> getKeyToFinger() {
        return keyToFinger;
    }

    private double getFadeFactor(int delay) {
        if (delay < fadeFactors.length) {
            return fadeFactors[delay];
        } else {
            return fadeFactors[fadeFactors.length - 1];
        }
    }
    
}
