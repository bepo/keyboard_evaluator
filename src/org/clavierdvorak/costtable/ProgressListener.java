/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

/**
 *
 * @author twilight
 */
public interface ProgressListener<T> {
    
    public void progressUpdated(T t);
    
}
