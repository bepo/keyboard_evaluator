/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.clavierdvorak.Key;
import org.clavierdvorak.keyselector.ClassicKeyboardKeySelector;

/**
 *
 * @author twilight
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        FileInputStream in = new FileInputStream("costs.cfg");
        final CostConfiguration conf = new CostConfiguration(in);
        in.close();
        
        List<Key> leftHandKeys = new ArrayList<Key>(conf.getLeftHandKeys().keySet());
        List<Key> rightHandKeys = new ArrayList<Key>(conf.getRightHandKeys().keySet());
        long time = System.currentTimeMillis();
        /*
        final RuleBasedBuilder builder = new RuleBasedBuilder(conf, new Function<Key, Key, Double>() {
            //Map<Key, Finger> keyToFinger = KeyToFinger.getClassicalKeyToFinger();
            KlayHand hand = new KlayHand();
            int[] toKlayFinger = {0,1,2,3,4,0,1,2,3,4};
            public Double eval(Key first, Key second) {
                final Finger f1 = conf.keyToFinger(first);//keyToFinger.get(first);
                final Finger f2 = conf.keyToFinger(second);//keyToFinger.get(second);
                //System.err.println(first+" "+second+" "+f1+" "+f2);
                // init using klay
                if (f1.isIsRightHand() != f2.isIsRightHand()) {
                    System.err.println("SHOULD NOT");
                    return (double) hand.paramAdditionToFinger;
                } else {
                    Map<Key, FingerInfo> map = f1.isIsRightHand() ? KlayTyper.rightFingers : KlayTyper.leftFingers;
                    hand.reinit();
                    hand.moveFinger(toKlayFinger[f1.getUniqueIndex()], map.get(first).fingerPosition);
                    // and ignore this cost
                    hand.moveFinger(toKlayFinger[f2.getUniqueIndex()], map.get(second).fingerPosition);
                    // and return this second cost
                    return (double) hand.currentCost();
                }
            }
        }, leftHandKeys, rightHandKeys);
         /*/
        final RuleBasedBuilder builder = new RuleBasedBuilder(conf)
                //.rules(conf.getRules())
                //.rule("if ($1.isLeftHand()) value -= 0.5")
                //.rule("if ($1.isLeftHand() && $2.isRightHand()) value *= 1.1")
                ;
        //*/
        
        /*
         * This worker class is only used to test workers in this context.
         */
        final JFrame pf = frame(new JProgressBar());
        class Worker extends SwingWorker<DikeyTableTyper, Double> implements ProgressListener<Double> {
            @Override
            protected DikeyTableTyper doInBackground() throws Exception {
                return builder.build(this);
            }
            int progressCount = 0;
            public void progressUpdated(Double progress) {
                publish((progressCount++)*100 + progress);
            }

            @Override
            protected void process(List<Double> list) {
                Double v = list.get(list.size() - 1);
                //System.err.println((int) (v / 100) + " : " + (v - 100 * ((int) (v / 100))));
                ((JProgressBar)pf.getContentPane()).setValue((int) (v / 100));
            }
            
        }
        Worker worker = new Worker();
        worker.execute();
        DikeyTableTyper typer = worker.get();

        System.err.println("time: "+(System.currentTimeMillis()-time)+" ms");
        time = System.currentTimeMillis();
        
        ClassicKeyboardKeySelector keySelector = new ClassicKeyboardKeySelector("64.xkb");
        Reader characterReader = new BufferedReader(new InputStreamReader(new FileInputStream("corpus-klay-clean.txt"), "utf-8"));
        Iterator<Key> keySequence = keySelector.getKeySequenceFor(characterReader);
        DikeyTableStatistic stat = (DikeyTableStatistic) typer.typeKeySequence(keySequence);

        System.err.println("time: "+(System.currentTimeMillis()-time)+" ms");
        System.err.println("alt:   "+stat.alternanceCount);
        System.err.println("Icost: "+stat.unikeyCost);
        System.err.println("Lcost: "+stat.leftHandSeparateCost);
        System.err.println("Rcost: "+stat.rightHandSeparateCost);
        
        for (Map<Key, CostConfiguration.KeyInformation> keyGroup : builder.getGroups()) {
            // View dikey table content
            JFrame f = new JFrame("DikeyTable");
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            TableModel model;
            {
                int dim = keyGroup.size();
                Double[][] table = new Double[dim][dim];
                String[] header = new String[dim];
                int i1 = 0;
                for (Key k1 : keyGroup.keySet()) {
                    header[i1] = k1.getScancodeString();
                    int i2 = 0;
                    for (Key k2 : keyGroup.keySet()) {
                        table[i1][i2] = typer.getCosts().get(k1).get(k2);
                        i2++;
                    }
                    i1++;
                }
                model = new DefaultTableModel(table, header);
            }
            f.setContentPane(new JScrollPane(new JTable(model)));
            f.setPreferredSize(new Dimension(1200, 1000));
            f.pack();
            f.setVisible(true);
        }
    }

    private static JFrame frame(JComponent content) {
        JFrame frame = new JFrame();
        frame.setContentPane(content);
        frame.pack();
        frame.setVisible(true);
        return frame;
    }

}
