/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import org.clavierdvorak.Conversion;
import org.clavierdvorak.Key;

import static org.clavierdvorak.costtable.Finger.*;

/**
 *
 * @author twilight
 */
@Deprecated
public class KeyToFinger {
    
    private static final Map<Key, Finger> classicalKeyToFinger = new TreeMap<Key, Finger>();
    
    private static class Filler {
        Map<Key, Finger> map;
        Filler(Map<Key, Finger> map) {
            this.map = map;
        }
        Filler put(String xkb, Finger f) {
            map.put(Conversion.xkbToScancode.get(xkb), f);
            return this;
        }
    }
    
    static {
        new Filler(classicalKeyToFinger)
                .put("TLDE", LeftLittleFinger)
                .put("AE01", LeftLittleFinger)
                .put("AE02", LeftLittleFinger)
                .put("AE03", LeftRingFinger)
                .put("AE04", LeftMiddleFinger)
                .put("AE05", LeftIndexFinger)
                .put("AE06", LeftIndexFinger)
                
                .put("AD01", LeftLittleFinger)
                .put("AD02", LeftRingFinger)
                .put("AD03", LeftMiddleFinger)
                .put("AD04", LeftIndexFinger)
                .put("AD05", LeftIndexFinger)
                
                .put("AC01", LeftLittleFinger)
                .put("AC02", LeftRingFinger)
                .put("AC03", LeftMiddleFinger)
                .put("AC04", LeftIndexFinger)
                .put("AC05", LeftIndexFinger)
                
                .put("LSGT", LeftLittleFinger)
                .put("AB01", LeftLittleFinger)
                .put("AB02", LeftRingFinger)
                .put("AB03", LeftMiddleFinger)
                .put("AB04", LeftIndexFinger)
                .put("AB05", LeftIndexFinger)
                
                .put("SPCE", LeftThumb)
                .put("LFSH", LeftLittleFinger)
                
                .put("AE07", RightIndexFinger)
                .put("AE08", RightIndexFinger)
                .put("AE09", RightMiddleFinger)
                .put("AE10", RightRingFinger)
                .put("AE11", RightLittleFinger)
                .put("AE12", RightLittleFinger)
                
                .put("AD06", RightIndexFinger)
                .put("AD07", RightIndexFinger)
                .put("AD08", RightMiddleFinger)
                .put("AD09", RightRingFinger)
                .put("AD10", RightLittleFinger)
                .put("AD11", RightLittleFinger)
                .put("AD12", RightLittleFinger)
                
                .put("AC06", RightIndexFinger)
                .put("AC07", RightIndexFinger)
                .put("AC08", RightMiddleFinger)
                .put("AC09", RightRingFinger)
                .put("AC10", RightLittleFinger)
                .put("AC11", RightLittleFinger)
                .put("BKSL", RightLittleFinger)
                
                .put("AB06", RightIndexFinger)
                .put("AB07", RightIndexFinger)
                .put("AB08", RightMiddleFinger)
                .put("AB09", RightRingFinger)
                .put("AB10", RightLittleFinger)
                
                .put("SPCE", RightThumb)
                .put("RTRN", RightLittleFinger)
                .put("RTSH", RightLittleFinger)
                .put("RALT", RightThumb);
        
        
    }

    public static Map<Key, Finger> getClassicalKeyToFinger() {
        return Collections.unmodifiableMap(classicalKeyToFinger);
    }

    
}
