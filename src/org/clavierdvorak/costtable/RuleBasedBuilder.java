/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.SwingWorker;
import org.clavierdvorak.Conversion;
import org.clavierdvorak.Key;
import org.clavierdvorak.costtable.CostConfiguration.KeyInformation;

/**
 *
 * @author twilight
 */
public class RuleBasedBuilder {
    
    private Double initialValue = null;
    private Function<Key,Key,Double> initialValueFunction = null;
    private List<String> rules = new ArrayList<String>();
    private CostConfiguration configuration;
    
    public RuleBasedBuilder(CostConfiguration configuration) {
        this.configuration = configuration;
        rules.addAll(configuration.getRules());
    }

    public RuleBasedBuilder(CostConfiguration configuration, double initialValue) {
        this(configuration);
        this.initialValue = initialValue;
    }
    
    public RuleBasedBuilder(CostConfiguration configuration, Function<Key, Key, Double> initialValueFunction) {
        this(configuration);
        this.initialValueFunction = initialValueFunction;
    }

    public RuleBasedBuilder rule(String rule) {
        rules.add(rule);
        return this;
    }

    public RuleBasedBuilder rules(Collection<String> rule) {
        for (String r : rule) {
            rules.add(r);
        }
        return this;
    }
    
    public DikeyTableTyper build() {
        return build(null);
    }
    
    public DikeyTableTyper build(final ProgressListener<Double> progressListenerOrNull) {
        DikeyTableTyper res = new DikeyTableTyper();
        res.setFadeFactors(configuration.fadeFactors);
        final ScriptEngineManager mgr = new ScriptEngineManager();
        final ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");
        final KeyTools k1 = new KeyTools();
        final KeyTools k2 = new KeyTools();
        final int keyCount = Conversion.scancodeToAzerty.keySet().size(); // todo clean progress
        final Set<Key> encounteredKeys = Key.set();
        jsEngine.put("$1", k1);
        jsEngine.put("$2", k2);
        
        StringBuilder mergedRulesBuilder = new StringBuilder();
        for (String rule : rules) {
            mergedRulesBuilder.append(rule).append("\n");
        }
        final String mergedRules = mergedRulesBuilder.toString();
        final List<Map<Key, KeyInformation>> keyGroups = getGroups();
        {
            Map<Key, Finger> keyToFinger = Key.indexedMap();
            for (Map<Key, KeyInformation> group : keyGroups) {
                for (Map.Entry<Key, KeyInformation> e : group.entrySet()) {
                    keyToFinger.put(e.getKey(), e.getValue().finger);
                }
            }
            res.setKeyToFinger(keyToFinger);
        }
        for (Map<Key, CostConfiguration.KeyInformation> keyGroup : keyGroups) {
            for (Map.Entry<Key, CostConfiguration.KeyInformation> e : keyGroup.entrySet()) {
                res.setUnikeyCost(e.getKey(), e.getValue().cost);
            }
            if (initialValueFunction == null && initialValue == null) {
                initialValue = 0.;
            }
            final boolean isRightHandGroup = keyGroup.entrySet().iterator().next().getValue().finger.isIsRightHand();
            res.initAtValue(keyGroup.keySet(), new Function<Key, Key, Double>() {
                public Double eval(Key first, Key second) {
                    if (progressListenerOrNull != null) {
                        int sizeBefore = encounteredKeys.size();
                        encounteredKeys.add(first);
                        int sizeAfter = encounteredKeys.size();
                        if (sizeBefore != sizeAfter) {
                            progressListenerOrNull.progressUpdated((double) sizeAfter / (double) keyCount);
                        }
                    }
                    double res = initialValueFunction == null ? initialValue : initialValueFunction.eval(first, second);
                    jsEngine.put("value", res);
                    if (first.equals(configuration.spaceKey)) {
                        k1.set(first, isRightHandGroup ? configuration.rightSpace : configuration.leftSpace);
                    } else {
                        k1.set(first, isRightHandGroup ? configuration.rightHandKeys.get(first) : configuration.leftHandKeys.get(first));
                    }
                    if (second.equals(configuration.spaceKey)) {
                        k2.set(second, isRightHandGroup ? configuration.rightSpace : configuration.leftSpace);
                    } else {
                        k2.set(second, isRightHandGroup ? configuration.rightHandKeys.get(second) : configuration.leftHandKeys.get(second));
                    }
                    if (!rules.isEmpty()) {
                        try {
                            jsEngine.eval(mergedRules);
                        } catch (ScriptException ex) {
                            Logger.getLogger(RuleBasedBuilder.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    res = (Double) jsEngine.get("value");
                    return res;
                }
            });
        }
        return res;
    }
    
    public SwingWorker<Void, Double> worker() {
        class Res extends SwingWorker<Void, Double> implements ProgressListener<Double> {
            @Override
            protected Void doInBackground() throws Exception {
                build(this);
                return null;
            }
            public void progressUpdated(Double progress) {
                publish(progress);
            }
        }
        return new Res();
    }

    List<Map<Key, KeyInformation>> getGroups() {
        List<Map<Key, CostConfiguration.KeyInformation>> keyGroups = new ArrayList<Map<Key, CostConfiguration.KeyInformation>>();
        {
            Map<Key, CostConfiguration.KeyInformation> group;
            group = Key.indexedSortedMap();
            group.putAll(configuration.leftHandKeys);
            group.put(configuration.spaceKey, configuration.leftSpace);
            keyGroups.add(group);
            group = Key.indexedSortedMap();
            group.putAll(configuration.rightHandKeys);
            group.put(configuration.spaceKey, configuration.rightSpace);
            keyGroups.add(group);
        }
        return keyGroups;
    }
    
    
}
