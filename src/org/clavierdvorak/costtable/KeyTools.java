package org.clavierdvorak.costtable;

import org.clavierdvorak.Key;

final class KeyTools {

    Key key;
    CostConfiguration.KeyInformation keyInformation;
    Finger finger;

    public boolean isLeftHand() {
        return !finger.isIsRightHand();
    }

    public boolean isRightHand() {
        return finger.isIsRightHand();
    }
    
    public boolean isKey(String scancodeString) {
        return key.getScancodeString().equals(scancodeString);
    }
    
    public boolean isI() {
        return finger.getRomanNumber().equals("I");
    }

    public boolean isII() {
        return finger.getRomanNumber().equals("II");
    }

    public boolean isIII() {
        return finger.getRomanNumber().equals("III");
    }

    public boolean isIV() {
        return finger.getRomanNumber().equals("IV");
    }

    public boolean isV() {
        return finger.getRomanNumber().equals("V");
    }
    
    public int fingerNumber() {
        return finger.getNumber();
    }
    
    public int row() {
        return keyInformation.row;
    }

    public int col() {
        return keyInformation.column;
    }

    public int column() {
        return keyInformation.column;
    }

    void set(Key key, CostConfiguration.KeyInformation keyInformation) {
        this.key = key;
        this.keyInformation = keyInformation;
        this.finger = keyInformation.finger;
    }
    
}
