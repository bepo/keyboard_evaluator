/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.costtable;

/**
 *
 * @author twilight
 */
public enum Finger {
    
    LeftThumb(0, false, "I"),
    LeftIndexFinger(1, false, "II"),
    LeftMiddleFinger(2, false, "III"),
    LeftRingFinger(3, false, "IV"),
    LeftLittleFinger(4, false, "V"),
    
    RightThumb(5, true, "I"),
    RightIndexFinger(6, true, "II"),
    RightMiddleFinger(7, true, "III"),
    RightRingFinger(8, true, "IV"),
    RightLittleFinger(9, true, "V"),
      ;
    
    public static int size() {
        return Finger.values().length;
    }
    
    private int uniqueIndex;
    private boolean isRightHand;
    private String romanNumber;

    public boolean isIsRightHand() {
        return isRightHand;
    }

    public String getRomanNumber() {
        return romanNumber;
    }
    
    public int getNumber() {
        return uniqueIndex % 5 + 1;
    }

    public int getUniqueIndex() {
        return uniqueIndex;
    }


    private Finger(int uniqueIndex, boolean isRightHand, String romanNumber) {
        this.uniqueIndex = uniqueIndex;
        this.isRightHand = isRightHand;
        this.romanNumber = romanNumber;
    }

    @Override
    public String toString() {
        return "Finger(" + uniqueIndex + ", " + (isRightHand ? "Right" : "Left") + "-" + romanNumber+")";
    }
        
}
