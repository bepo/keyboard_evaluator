package org.clavierdvorak.layoutcomparator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.clavierdvorak.Conversion;
import org.clavierdvorak.Key;
import org.clavierdvorak.Statistic;
import org.clavierdvorak.klaylike.KlayHand;
import org.clavierdvorak.klaylike.KlayStatistic;
import org.clavierdvorak.klaylike.KlayTyper;
import org.clavierdvorak.klaylike.KlayTyper.FingerInfo;

public class CountTyper extends KlayTyper {

    // TODO: chargement depuis un fichier
    public static Map<Key, Integer> accessibilityMap = new HashMap<Key, Integer>() {{
        put(Conversion.xkbToScancode.get("TLDE"), new Integer(2));
        put(Conversion.xkbToScancode.get("AE01"), new Integer(7));
        put(Conversion.xkbToScancode.get("AE02"), new Integer(11));
        put(Conversion.xkbToScancode.get("AE03"), new Integer(21));
        put(Conversion.xkbToScancode.get("AE04"), new Integer(23));
        put(Conversion.xkbToScancode.get("AE05"), new Integer(14));
        put(Conversion.xkbToScancode.get("AE06"), new Integer(2));
        put(Conversion.xkbToScancode.get("AE07"), new Integer(16));
        put(Conversion.xkbToScancode.get("AE08"), new Integer(22));
        put(Conversion.xkbToScancode.get("AE09"), new Integer(23));
        put(Conversion.xkbToScancode.get("AE10"), new Integer(18));
        put(Conversion.xkbToScancode.get("AE11"), new Integer(6));
        put(Conversion.xkbToScancode.get("AE12"), new Integer(2));
        put(Conversion.xkbToScancode.get("AD01"), new Integer(60));
        put(Conversion.xkbToScancode.get("AD02"), new Integer(65));
        put(Conversion.xkbToScancode.get("AD03"), new Integer(75));
        put(Conversion.xkbToScancode.get("AD04"), new Integer(68));
        put(Conversion.xkbToScancode.get("AD05"), new Integer(44));
        put(Conversion.xkbToScancode.get("AD06"), new Integer(34));
        put(Conversion.xkbToScancode.get("AD07"), new Integer(67));
        put(Conversion.xkbToScancode.get("AD08"), new Integer(75));
        put(Conversion.xkbToScancode.get("AD09"), new Integer(70));
        put(Conversion.xkbToScancode.get("AD10"), new Integer(53));
        put(Conversion.xkbToScancode.get("AD11"), new Integer(38));
        put(Conversion.xkbToScancode.get("AD12"), new Integer(16));
        put(Conversion.xkbToScancode.get("AC01"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC02"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC03"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC04"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC05"), new Integer(62));
        put(Conversion.xkbToScancode.get("AC06"), new Integer(63));
        put(Conversion.xkbToScancode.get("AC07"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC08"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC09"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC10"), new Integer(100));
        put(Conversion.xkbToScancode.get("AC11"), new Integer(57));
        put(Conversion.xkbToScancode.get("BKSL"), new Integer(19));

//Valeurs dérivées de la partie droite, pour les deux dispositions
//Dactylo
        put(Conversion.xkbToScancode.get("LSGT"), new Integer(49));
        put(Conversion.xkbToScancode.get("AB01"), new Integer(62));
        put(Conversion.xkbToScancode.get("AB02"), new Integer(52));
        put(Conversion.xkbToScancode.get("AB03"), new Integer(57));
        put(Conversion.xkbToScancode.get("AB04"), new Integer(73));
// A_Shape
/*
        put(Conversion.xkbToScancode.get("LSGT"), new Integer(62));
        put(Conversion.xkbToScancode.get("AB01"), new Integer(52));
        put(Conversion.xkbToScancode.get("AB02"), new Integer(57));
        put(Conversion.xkbToScancode.get("AB03"), new Integer(73));
        put(Conversion.xkbToScancode.get("AB04"), new Integer(62));
*/
        put(Conversion.xkbToScancode.get("AB05"), new Integer(28));
        put(Conversion.xkbToScancode.get("AB06"), new Integer(62));
        put(Conversion.xkbToScancode.get("AB07"), new Integer(73));
        put(Conversion.xkbToScancode.get("AB08"), new Integer(57));
        put(Conversion.xkbToScancode.get("AB09"), new Integer(52));
        put(Conversion.xkbToScancode.get("AB10"), new Integer(62));
        put(Conversion.xkbToScancode.get("SPCE"), new Integer(100));

        // Some guesswork - unused here anyway: TODO: find how to combine chars
        put(Conversion.xkbToScancode.get("LFSH"), new Integer(20));
        put(Conversion.xkbToScancode.get("RTRN"), new Integer(10));
        put(Conversion.xkbToScancode.get("RTSH"), new Integer(25));
        put(Conversion.xkbToScancode.get("RALT"), new Integer(80));

    }};

    // TODO: chargement depuis un fichier
    /*
		0: natural position
		1: shifted index, needs +2 finger delta
		2: medium difficulty position
		3: difficult 
     */
    public static Map<Key, Integer> digramAccessibilityMap = new HashMap<Key, Integer>() {{
        put(Conversion.xkbToScancode.get("TLDE"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE01"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE02"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE03"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE04"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE05"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE06"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE07"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE08"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE09"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE10"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE11"), new Integer(3));
        put(Conversion.xkbToScancode.get("AE12"), new Integer(3));
        put(Conversion.xkbToScancode.get("AD01"), new Integer(2));
        put(Conversion.xkbToScancode.get("AD02"), new Integer(0));
        put(Conversion.xkbToScancode.get("AD03"), new Integer(0));
        put(Conversion.xkbToScancode.get("AD04"), new Integer(0));
        put(Conversion.xkbToScancode.get("AD05"), new Integer(2));
        put(Conversion.xkbToScancode.get("AD06"), new Integer(2));
        put(Conversion.xkbToScancode.get("AD07"), new Integer(0));
        put(Conversion.xkbToScancode.get("AD08"), new Integer(0));
        put(Conversion.xkbToScancode.get("AD09"), new Integer(0));
        put(Conversion.xkbToScancode.get("AD10"), new Integer(2));
        put(Conversion.xkbToScancode.get("AD11"), new Integer(2));
        put(Conversion.xkbToScancode.get("AD12"), new Integer(3));
        put(Conversion.xkbToScancode.get("AC01"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC02"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC03"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC04"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC05"), new Integer(1));
        put(Conversion.xkbToScancode.get("AC06"), new Integer(1));
        put(Conversion.xkbToScancode.get("AC07"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC08"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC09"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC10"), new Integer(0));
        put(Conversion.xkbToScancode.get("AC11"), new Integer(2));
        put(Conversion.xkbToScancode.get("BKSL"), new Integer(3));
        // Dactylo
        put(Conversion.xkbToScancode.get("LSGT"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB01"), new Integer(0));
        put(Conversion.xkbToScancode.get("AB02"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB03"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB04"), new Integer(0));
        put(Conversion.xkbToScancode.get("AB05"), new Integer(3));
        // A_Shape
/*

        put(Conversion.xkbToScancode.get("LSGT"), new Integer(0));
        put(Conversion.xkbToScancode.get("AB01"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB02"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB03"), new Integer(0));
        put(Conversion.xkbToScancode.get("AB04"), new Integer(1));
        put(Conversion.xkbToScancode.get("AB05"), new Integer(3));
*/
        put(Conversion.xkbToScancode.get("AB06"), new Integer(1));
        put(Conversion.xkbToScancode.get("AB07"), new Integer(0));
        put(Conversion.xkbToScancode.get("AB08"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB09"), new Integer(2));
        put(Conversion.xkbToScancode.get("AB10"), new Integer(0));
        put(Conversion.xkbToScancode.get("SPCE"), new Integer(0));

        put(Conversion.xkbToScancode.get("LFSH"), new Integer(3));
        put(Conversion.xkbToScancode.get("RTRN"), new Integer(3));
        put(Conversion.xkbToScancode.get("RTSH"), new Integer(3));
        put(Conversion.xkbToScancode.get("RALT"), new Integer(2));

    }};

    public void setHandPlacement(HandPlacement placement) {
    	super.setHandPlacement(placement);
    	switch (placement) {
    		case Dactylo: {
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("LSGT"), new Integer(2));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB01"), new Integer(0));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB02"), new Integer(2));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB03"), new Integer(2));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB04"), new Integer(0));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB05"), new Integer(3));
    			accessibilityMap.put(Conversion.xkbToScancode.get("LSGT"), new Integer(49));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB01"), new Integer(62));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB02"), new Integer(52));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB03"), new Integer(57));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB04"), new Integer(73));
    			break;
    		}
    		case A_Shape: {
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("LSGT"), new Integer(0));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB01"), new Integer(2));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB02"), new Integer(2));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB03"), new Integer(0));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB04"), new Integer(1));
    			digramAccessibilityMap.put(Conversion.xkbToScancode.get("AB05"), new Integer(3));
    			accessibilityMap.put(Conversion.xkbToScancode.get("LSGT"), new Integer(62));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB01"), new Integer(52));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB02"), new Integer(57));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB03"), new Integer(73));
    			accessibilityMap.put(Conversion.xkbToScancode.get("AB04"), new Integer(62));
    			break;
    		}
    	}
    }

    // Suppose que les doigts sont sur la même main
    // Renvoie vrai si:
    // - pouces non inclus
    // - les doigts sont à >= mindelta l'un de l'autre (pas de digramme à un doigt)
    // - les doigts sont à au plus une rangée d'écart
    protected boolean doigtDigramme(FingerInfo o, FingerInfo n, int mindelta) {
    	// pouces non inclus
    	if (o.finger == 0 || n.finger == 0) return false;
		// Au plus une rangée d'écart
		if (Math.abs(o.fingerPosition[1] - n.fingerPosition[1]) > 5) return false;
		// Au moins l'écart spécifié  
		if (Math.abs(o.finger - n.finger) < mindelta) return false; 
    	return true;
    }

    protected void maintainStats(KlayStatistic stat, Key key, FingerInfo f, KlayHand hand) {
    	CountStatistic cst = (CountStatistic)stat;
    	
    	Integer digNum = digramAccessibilityMap.get(key);
    	Integer prevDigNum = digramAccessibilityMap.get(previousKey);

    	if (digNum==null || prevDigNum==null) return;

        if (previousFinger.finger == f.finger) {
            if (previousKey.scancode == key.scancode) ++cst.totalConsecutiveSameFingerSameKey;
            else if (previousHand == hand) ++cst.totalConsecutiveSameFingerDifferentKey;
        }
    	
    	Integer access = accessibilityMap.get(key);
    	if (access!=null) cst.accessible += access.longValue();
    	if (f.finger!=0 && previousFinger.finger!=0 && previousHand == hand && f.finger < previousFinger.finger) ++cst.versInterieur;

    	int digV = digNum.intValue();
    	int prevDigV = prevDigNum.intValue();
    	boolean difficult = true;

        if (f.finger == 0 || previousFinger.finger == 0) {
            // l'un ou l'autre a digram value de 0: somme = max
            if (digV+prevDigV < 2) {
                // pouce tjs à écart min
		++cst.digrammesFacilesSansAlternance;
		++cst.digrammesFacilesIncluantAlternance;
		difficult = false;
            }
            else if (digV+prevDigV == 2) {
                // pouce tjs à écart min
		++cst.digrammesMoyensIncluantAlternance;
		++cst.digrammesMoyensSansAlternance;
		difficult = false;
            }
        } else {

            if (digV<2) {
                if (prevDigV==0 || (digV==0 && prevDigV==1)) {
                    // double frappe
                    if (previousKey.scancode == key.scancode) {
                        ++cst.digrammesFacilesSansAlternance;
                        ++cst.digrammesFacilesIncluantAlternance;
                        difficult = false;
                    }
                    else if (previousHand != hand) {
                        ++cst.digrammesFacilesIncluantAlternance; 
                        difficult = false;
                    }
                    else {
                        if (doigtDigramme(previousFinger, f, prevDigV+digV+1)) {
                            ++cst.digrammesFacilesIncluantAlternance; 
                            ++cst.digrammesFacilesSansAlternance;
                            difficult = false;
                        // sauf si 1 seul doigt d'écart alors que deux requis: moyen
                        } else if (prevDigV+digV==1 && doigtDigramme(previousFinger, f, 1)) {
                            ++cst.digrammesMoyensIncluantAlternance; 
                            ++cst.digrammesMoyensSansAlternance;
                            difficult = false;
                        }
                    }
                }
            }
            if ((digV==2 && prevDigV<3) || (digV<3 && prevDigV==2)) {
                // double frappe
                if (previousKey.scancode == key.scancode) {
                    ++cst.digrammesMoyensIncluantAlternance;
                    ++cst.digrammesMoyensSansAlternance;
                    difficult = false;
                }
                else if (previousHand != hand) {
                    ++cst.digrammesMoyensIncluantAlternance;
                    difficult = false;
                }
                else if (doigtDigramme(previousFinger, f, 1)) {
                    ++cst.digrammesMoyensIncluantAlternance;
                    ++cst.digrammesMoyensSansAlternance;
                    difficult = false;
                }
            }

    	}
    	
    	if (difficult) ++cst.digrammesDifficiles;
    	
    	++cst.numDigrammes;
    }
    
    public Statistic typeKeySequence(Iterator<Key> keySequence) {
    	return typeKeySequence(keySequence, new CountStatistic());
    }

}
