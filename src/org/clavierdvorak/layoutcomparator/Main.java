package org.clavierdvorak.layoutcomparator;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.util.Arrays;
import java.util.Iterator;
import java.util.SortedMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
//Java6 only
//import javax.swing.SwingWorker;
import org.jdesktop.swingworker.SwingWorker;
import javax.swing.UIManager;

import org.clavierdvorak.CorpusStatistic;
import org.clavierdvorak.Key;
import org.clavierdvorak.KeySelector;
import org.clavierdvorak.Statistic;
import org.clavierdvorak.keyselector.ClassicKeyboardKeySelector;
import org.clavierdvorak.klaylike.KlayStatistic;
import org.clavierdvorak.klaylike.KlayTyper;


/**
* @author labnico
*/
public class Main extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	JButton layoutBeforeButton, layoutAfterButton, corpusButton, calculeButton;
    JFileChooser fc;
    JTextArea log;
    File layoutBeforeFile, layoutAfterFile, corpusFile;
    JComboBox encodingsCombo, placementCombo;
	JPanel encodingPanel;
    String encoding;
    JCheckBox silentCB;
    boolean silentCompute, silent;
    KlayTyper.HandPlacement handPlacement = KlayTyper.HandPlacement.Dactylo;
    
    CountStatistic[] stat;
	public static Main application; 
	
	public Main() {
		super("Comparateur de Layout");
	}

	byte[] bufferedBytes = new byte[10];
	int bufferedBytesLen = 0;
	PrintStream systemOut;
	
	public void GUI() throws UnsupportedEncodingException {
        log = new JTextArea(40,80);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);
        PrintStream redirectedOutErr = new PrintStream(new OutputStream() {
        	public void write(int b) throws IOException {
        		if (silent) return;
        		bufferedBytes[bufferedBytesLen++] = (byte)b;
        		if (bufferedBytesLen >= bufferedBytes.length) {
        			byte[] bufferedBytes2 = new byte[bufferedBytesLen*2];
        			for (int i=0; i<bufferedBytesLen; ++i) bufferedBytes2[i] = bufferedBytes[i];
/*        			try {
        				System.arraycopy(bufferedBytes, 0, bufferedBytes2, 0, bufferedBytesLen);
        			} catch (Exception e) {
        				// Java6 feature;
        				throw new IOException(e);
        			}
*/
        			bufferedBytes = bufferedBytes2;
        		}
        	}
        	public void flush() throws IOException {
        		if (silent) return;
       			byte[] msg = new byte[bufferedBytesLen];
       			for (int i=0; i<bufferedBytesLen; ++i) msg[i] = bufferedBytes[i];
           		log.append(new String(msg, "UTF-8"));
           		// Java6 feature
           		// log.append(new String(Arrays.copyOf(bufferedBytes,bufferedBytesLen), "UTF-8"));
           		log.setCaretPosition(log.getDocument().getLength());
           		bufferedBytesLen = 0;
        		super.flush();
        	}
        },true,"UTF-8");
        systemOut = System.out;
        System.setOut(redirectedOutErr);
        System.setErr(redirectedOutErr);
        JScrollPane logScrollPane = new JScrollPane(log);
        fc = new JFileChooser();
        layoutBeforeButton = new JButton("Layout Original");
        layoutAfterButton = new JButton("Layout Modifié");
        corpusButton = new JButton("Corpus");
        calculeButton = new JButton("Calcule!");
        silentCB = new JCheckBox("Silencieux");
        layoutBeforeButton.addActionListener(this);
        layoutAfterButton.addActionListener(this);
        corpusButton.addActionListener(this);
        calculeButton.addActionListener(this);
        silentCB.addActionListener(this);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(layoutBeforeButton);
        buttonPanel.add(layoutAfterButton);
        buttonPanel.add(corpusButton);
        buttonPanel.add(calculeButton);
        buttonPanel.add(silentCB);
		placementCombo = new JComboBox(KlayTyper.HandPlacement.values());
		placementCombo.setPreferredSize(placementCombo.getMinimumSize());
		placementCombo.setMaximumSize(placementCombo.getMinimumSize());
		placementCombo.addActionListener(this);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(new JLabel("Placement doigts "));
		buttonPanel.add(placementCombo);
        //Add the buttons and the log to this panel.
		getContentPane().add(buttonPanel, BorderLayout.PAGE_START);
		getContentPane().add(logScrollPane, BorderLayout.CENTER);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
        if (e.getSource() == layoutBeforeButton) {
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                layoutBeforeFile = fc.getSelectedFile();
                System.out.println("Layout Original : " + layoutBeforeFile.getAbsolutePath());
            }
            return;
        }
        if (e.getSource() == layoutAfterButton) {
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                layoutAfterFile = fc.getSelectedFile();
                System.out.println("Layout Modifié : " + layoutAfterFile.getAbsolutePath());
            }
            return;
        }
        if (e.getSource() == corpusButton) {
        	if (encodingsCombo==null) {

        		SortedMap<String,Charset> cs = Charset.availableCharsets();
        		String[] encodings = cs.keySet().toArray(new String[0]);//{ "UTF-8", "ISO-8859-1", "CP1252" };
        		encodingsCombo = new JComboBox(encodings);
        		encodingsCombo.setEditable(true);
        		encoding = "UTF-8";
        		encodingsCombo.setSelectedItem(encoding);
        		encodingsCombo.addActionListener(this);
        		encodingPanel = new JPanel();
        		encodingPanel.setLayout(new BoxLayout(encodingPanel, BoxLayout.Y_AXIS));
        		encodingsCombo.setPreferredSize(encodingsCombo.getMinimumSize());
        		encodingsCombo.setMaximumSize(encodingsCombo.getMinimumSize());
        		encodingPanel.add(new JLabel("Encoding"));
        		encodingPanel.add(encodingsCombo);
        		encodingPanel.add(Box.createVerticalGlue());
        		
        	}
        	fc.setAccessory(encodingPanel);
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
            	corpusFile = fc.getSelectedFile();
                System.out.println("Corpus : " + corpusFile.getAbsolutePath() + " with encoding "+encoding);
            }
        	fc.setAccessory(null);
            return;
        }
        if (e.getSource() == silentCB) {
            silentCompute = silentCB.isSelected();
            return;
        }
        if (e.getSource() == encodingsCombo) {
            encoding = (String)encodingsCombo.getSelectedItem();
        }
        if (e.getSource() == placementCombo) {
        	handPlacement = (KlayTyper.HandPlacement)placementCombo.getSelectedItem();
        	System.out.println("Placement des mains : "+handPlacement);
        }
        
        if (e.getSource() == calculeButton) {
// Java6 way, but using LGPL'd backported swingworker :)
        	new SwingWorker<Void, Void>() {
        		public Void doInBackground() {
                		calcule();
				return null;
        		}
        		public void done() {
        		}
        	}.execute();
        }
		
	}

	
	public void calcule()  {
		try {
			silent = silentCompute;
			KeySelector[] keySelector = new KeySelector[2];
			stat = new CountStatistic[2];
			CorpusStatistic[] corpusStat = new CorpusStatistic[2];
			for(int i=0; i<2; ++i) {
				File file = i==0 ? layoutBeforeFile : layoutAfterFile;
				keySelector[i] = new ClassicKeyboardKeySelector(file.getAbsolutePath());
				Reader characterReader = new BufferedReader(new InputStreamReader(new FileInputStream(corpusFile), encoding));
				Iterator<Key> keySequence = keySelector[i].getKeySequenceFor(characterReader);
				KlayTyper typer = new CountTyper();
				typer.setHandPlacement(handPlacement);
				stat[i] = (CountStatistic)typer.typeKeySequence(keySequence);
				//main.stat[i].processDerivedValues();
				corpusStat[i] = keySelector[i].getCorpusStatistic();
			}
			silent = false;

			System.out.println();
			System.out.println("Résultats du changement (en %) :");
			
			System.out.println("Charge des doigts (de gauche à droite, d'auriculaire à auriculaire)");
			System.out.print("ancien%  ");
			for (int i=4; i>=0; --i) {
				System.out.printf("%.2f",100.0f*(float)(stat[0].leftFingerCounts[i])/stat[0].totalFingerCount);
				System.out.print(" ");
			}
			for (int i=0; i<5; ++i) {
				System.out.printf("%.2f",100.0f*(float)(stat[0].rightFingerCounts[i])/stat[0].totalFingerCount);
				System.out.print(" ");
			}
			System.out.println();
			System.out.print("nouveau%  ");
			for (int i=4; i>=0; --i) {
				System.out.printf("%.2f",100.0f*(float)(stat[1].leftFingerCounts[i])/stat[1].totalFingerCount);
				System.out.print(" ");
			}
			for (int i=0; i<5; ++i) {
				System.out.printf("%.2f",100.0f*(float)(stat[1].rightFingerCounts[i])/stat[1].totalFingerCount);
				System.out.print(" ");
			}
			System.out.println();
			System.out.print("gain/perte-relative%  ");
			for (int i=4; i>=0; --i) {
				System.out.printf("%.2f",getPercentChange("leftFingerCounts",i));
				System.out.print(" ");
			}
			for (int i=0; i<5; ++i) {
				System.out.printf("%.2f",getPercentChange("rightFingerCounts",i));
				System.out.print(" ");
			}
			System.out.println();

			System.out.print("gain/perte-energy%  ");
			for (int i=4; i>=0; --i) {
				System.out.printf("%.2f",getPercentChange("leftFingerEnergies",i));
				System.out.print(" ");
			}
			for (int i=0; i<5; ++i) {
				System.out.printf("%.2f",getPercentChange("rightFingerEnergies",i));
				System.out.print(" ");
			}
			System.out.println();
			float absGain;
			System.out.printf("Energie : %+.2f%%\n",getPercentChange("totalEnergy"));
			System.out.printf("Accessibilité : %+.2f%%\n",getPercentChange("accessible"));
			System.out.println("Frappes vers l'intérieur : "+msgStatFor("versInterieur"));
			System.out.println("Digrammes faciles (hors alternance) : "+msgStatFor("digrammesFacilesSansAlternance"));
			System.out.println("Digrammes moyens (hors alternance) : "+msgStatFor("digrammesMoyensSansAlternance"));
			System.out.printf("Alternance brute : %+.2f%%\n",getPercentChange("alternancePerKey"));
			//System.out.printf("Digrammes 2 doigts touches contiguës même rangée : %+.2f%%\n",getPercentChange("sideBySideDigram"));
			System.out.println("Digrammes faciles (incluant alternance) : "+msgStatFor("digrammesFacilesIncluantAlternance"));
			System.out.println("Digrammes moyens (incluant alternance) : "+msgStatFor("digrammesMoyensIncluantAlternance"));
			System.out.println("Digrammes difficiles : "+msgStatFor("digrammesDifficiles"));
			System.out.println("dont digrammes 1 doigts (touches différentes) : "+msgStatFor("totalConsecutiveSameFingerDifferentKey"));
		}
		catch (Exception e) {
			e.printStackTrace();
			//System.out.println(e.getMessage());
		}
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		application = new Main();
		
		if (args.length<3) {
	        SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                UIManager.put("swing.boldMetal", Boolean.FALSE); 
	                try {
						application.GUI();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
	            }
	        });
			return;
		}
		//System.out.println("Arguments expected : layoutOriginal.xkb layoutModifié.xkb corpus.txt [encoding [placement(dactylo=0,A=1)]]");
		application.layoutBeforeFile = new File(args[0]);		
		application.layoutAfterFile = new File(args[1]);		
		application.corpusFile = new File(args[2]);
		application.encoding = args.length<4 ? "UTF-8" : args[3];
		if (args.length>4 && args[4].equals("1")) application.handPlacement = KlayTyper.HandPlacement.A_Shape;

		application.calcule();
	}

	float getPercentChange(String field) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Class<?> c = stat[0].getClass();
		Field f = c.getField(field);
		float before = f.getFloat(stat[0]);
		float after = f.getFloat(stat[1]);
		return 100.0f * (after - before) / before;
	}

	float getPercentChange(String field, int elt) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Class<?> c = stat[0].getClass();
		Field f = c.getField(field);
		float before = Array.getFloat(f.get(stat[0]), elt);
		float after = Array.getFloat(f.get(stat[1]), elt);
		return 100.0f * (after - before) / before;
	}

        String msgStatFor(String field) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
            Class<?> c = stat[0].getClass();
            Field f = c.getField(field);
            float before = f.getFloat(stat[0]);
            float after = f.getFloat(stat[1]);
            return String.format("(%d - %d) / %d = %+.2f%% absolu, %+.2f%% relatif",
                (long)after,
                (long)before,
                stat[0].numDigrammes,
                100.0f * (after - before) / stat[0].numDigrammes,
                100.0f * (after - before) / before
            );
        }
}
