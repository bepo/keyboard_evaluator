package org.clavierdvorak.layoutcomparator;

import org.clavierdvorak.klaylike.KlayStatistic;

public class CountStatistic extends KlayStatistic {

    public long accessible;
    public long versInterieur;
    public long digrammesFacilesSansAlternance;
    public long digrammesMoyensSansAlternance;
    public long digrammesFacilesIncluantAlternance;
    public long digrammesMoyensIncluantAlternance;
    public long digrammesDifficiles;
    public long numDigrammes;
    public int totalConsecutiveSameFingerSameKey;
    public int totalConsecutiveSameFingerDifferentKey;
    
/*  % accessibilité
	% vers l'intérieur
	% digrammes faciles (hors alternance)
	% digrammes moyens (hors alternance)
	% alternance brute
	% digrammes faciles (incluant alternance)
	% digrammes moyens (incluant alternance)
*/    	
}
