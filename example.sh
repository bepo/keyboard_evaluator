
# faire "ant jar" si l'on veut compiler le jar

# "time" -> chronométrer l'exécution
# "-Dfile.encoding=utf-8" -> pour afficher correctement dans les terminaux utf-8
# ajouter "-e iso-8859-1" si le corpus est en iso-8859-1 (marche avec n'importe quel charset)

time java -Dfile.encoding=utf-8 -jar dist/KeyboardEvaluator.jar -l 64.xkb -c extrait_001-utf.txt

# sur un corpus de 58k (corpus extrait_001 converti en utf-8 et en corrigeant les deux œ)

# bench java :
###  time java -Dfile.encoding=utf-8 -cp ../KeyboardEvaluator/build/classes/ keyboardevaluator.Main -l 64.xkb -c extrait_001-utf.txt
###  Initialisation
###  Arguments (2)
###    l : 64.xkb
###    c : extrait_001-utf.txt
###  Problem with xkb: probably xkb symbol 'U2013' is not found in our map
###  Problem with xkb: probably xkb symbol 'U0259' is not found in our map
###  Problem with xkb: probably xkb symbol 'Thorn' is not found in our map
###  Démarrage des calculs
###  Problem: no way to type character 'á' (xkb 'null') has been found ... skipped
###  Problem: no way to type character 'í' (xkb 'null') has been found ... skipped
###  Problem: no way to type character 'á' (xkb 'null') has been found ... skipped
###  Génération du rapport
###  0.752u 0.324s 0:01.02 104.9%    0+0k 0+0io 1pf+0w

# bench python :
# time python klay.py -l 64.xkb -c extrait_001-utf.txt
###  printing report...
###  done !
###  17.109u 0.064s 0:17.23 99.5%    0+0k 0+0io 0pf+0w


# autre mesure sur un corpus de 180k, encore pire pour python
# java : 1.48
# python : 53