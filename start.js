
// This script is responsible for configuring the runner object received.
// It has to set:
//  - a corpus file name (corpus)
//  - a corpus encoding (corpusEncoding), defaulting to "utf-8"
//  - a typer (typerFromKlay(), ...). The typer processes a key sequence and is
//    responsible to evaluate the cost of typing this key sequence and filling
//    stat object that will be processed by print.js.
//  - a key chooser (chooseMinimalKeyStrokesFromLayout(), ...). It is
//    responsible for converting a sequence of unicode characters (the corpus)
//    into a sequence of key stokes. It is based on the layout (xkb file) and
//    on a preferred sequence selector: some characters may be produced using
//    different key sequences, one is choosen by the key chooser.

// args: configuration map (extracted from command line)
// runner: object that will run the evaluation

println("Initialisation")
println("Arguments ("+args.size()+")")
var it = args.entrySet().iterator();
while (it.hasNext()) {
    var current = it.next();
    println("  "+current.key+(current.value!=null ? (" : "+current.value) : ""));
}

if (args.containsKey("help")) {
    println("Usage ...");
    java.lang.System.exit(0);
}

runner.typerFromKlay();

if (args.containsKey("c")) runner.corpus = args.get("c");
if (args.containsKey("e")) runner.corpusEncoding = args.get("e");
if (args.containsKey("l")) runner.chooseMinimalKeyStrokesFromLayout(args.get("l"));

println("Démarrage des calculs")
// List existing methods on object
//for (i in runner) println(i);

//var stat = runner.evaluate();
//for (i in stat) println(i);
