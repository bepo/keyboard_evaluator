
// corpusStat: statistic obtained by corpus parsing
// stat: statistic obtained by keymap evaluation
// tools: helper to build file printers ...

var reportFileName = "report.html"
if (args.containsKey("r")) reportFileName = args.get("r");
var svgFileName = reportFileName+".svg" // filename for svg layout file
if (args.containsKey("rs")) svgFileName = args.get("rs");
var report = tools.utf8File(reportFileName);

println("Génération du rapport")
//println(stat.getTotalPressedKeys());
//println(corpusStat.getTotalReadCharacters());
//bench without java.lang.System.exit(1);
report.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
report.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");

report.println("<html>")
report.println("<head>")
report.println('<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />')
report.println('<style lang="text/css">')
report.println(".overview {border: 2px solid black;}");
report.println(".overview td {padding: 5px;}");
report.println(".overview td:first-child {background: #8AF;}");
report.println(".overview td:first-child+td {background: #AEF;}");
report.println(".costs {border: 2px solid black;}");
report.println(".costs td {padding: 5px;}");
report.println(".costs td:first-child {background: #8AF;}");
report.println(".costs td:first-child+td {background: #AEF;}");
report.println(".costs td:first-child+td+td {background: #AEF;}");
report.println(".table2d { margin: 0; border-spacing: 0; padding: 0; border: 2px solid black;}");
report.println(".table2d td { padding:0; width: 0; height: 0;}");
report.println(".table2d td[title] { width: 6px; height: 6px;}");
report.println(".list {border: 1px solid black;}");
report.println(".list td {padding: 10px; padding-top:0; padding-bottom:0;}");
report.println(".charList td:first-child {text-align:center;}");
report.println(".charList td:first-child+td {text-align:right;}");
report.println(".charList td:first-child+td+td+td {text-align:right;}");
report.println(".keyList td:first-child {text-align:center;}");
report.println(".keyList td:first-child+td {text-align:center;}");
report.println(".keyList td:first-child+td+td {text-align:center;}");
report.println(".keyList td:first-child+td+td+td {text-align:right;}");
report.println(".listLine0 {background: #BFB;}");
report.println(".listLine1 {background: #FFB;}");
report.println(".listLine2 {background: #BBF;}");
report.println(".layout { border: 2px solid black;}");
report.println(".layout td { border: 1px solid black;}");
report.println(".key { display:inline; margin:0; padding-left:20px; border: 1px solid black;}");
report.println(".keytab { display:inline; margin:0; padding-left:35px; border: 1px solid black;}");
report.println("</style>")
report.println("</head>")

report.println("<body>")
report.println('<object type="image/svg+xml" data="'+svgFileName+'" width="837" height="285"></object>');
var svg = tools.utf8File(svgFileName);
tools.svgKeyboard(svg, runner.keySelector);
svg.close();

report.println("<h4>Statistiques</h4>")
report.println("<table class='overview'>")
tools.row(report, ["Fichier corpus", runner.corpus])
//tools.row(report, ["Extrait du corpus", tools.beginAndEndOfFile(40)]) not implemented yet
tools.row(report, ["Taille du corpus", corpusStat.getTotalReadCharacters()])
tools.row(report, ["Alternance des mains", stat.getAlternancePerKey()])
report.println("</table>")

report.println("<h4>Statistiques des coûts de Klay</h4>")
report.println("<table class='costs'>")
report.println("<thead><th></th><th>Gauche</th><th>Droite</th></thead>")
report.println("<tr><td colspan='3'></tr>")
tools.row(report, ["Paume", stat.getLeftPalmEnergy(), stat.getRightPalmEnergy()])
report.println("<tr><td colspan='3'></tr>")
tools.row(report, ["Doigts (tot)", stat.getLeftFingersEnergy(), stat.getRightFingersEnergy()])
tools.row(report, ["  Index", stat.getLeftFingerEnergies()[1], stat.getRightFingerEnergies()[1]])
tools.row(report, ["  Majeur", stat.getLeftFingerEnergies()[2], stat.getRightFingerEnergies()[2]])
tools.row(report, ["  Annulaire", stat.getLeftFingerEnergies()[3], stat.getRightFingerEnergies()[3]])
tools.row(report, ["  Auriculaire", stat.getLeftFingerEnergies()[4], stat.getRightFingerEnergies()[4]])
report.println("<tr><td colspan='3'></tr>")
tools.row(report, ["Total", stat.getLeftHandEnergy(), stat.getRightHandEnergy()])
report.println("</table>")


report.println("<h4>Fréquences des caractères</h4>")
tools.charFrequences(report, corpusStat.getReadCharacters(), corpusStat.getTotalReadCharacters(), 3, "charList", runner)

report.println("<h4>Fréquences des touches</h4>")
tools.keyFrequences(report, stat.getPressedKeys(), stat.getTotalPressedKeys(), 3, "keyList")

report.println("<h4>Digrammes de touches (Linéaire) (Logarithmique)</h4>")
report.println("<table><tr><td>")
tools.keyDigrams(report, stat.getConsecutivePressedKeys(), stat.getTotalPressedKeys(), false)
report.println("</td><td>")
//report.println("<h4>Digrammes de touches (Logarithmique)</h4>")
tools.keyDigrams(report, stat.getConsecutivePressedKeys(), stat.getTotalPressedKeys(), true)
report.println("</td></tr></table>")

report.println("<h4>Digrammes de caractères (Linéaire)</h4>")
tools.charDigrams(report, corpusStat.getReadDigrams(), corpusStat.getTotalReadCharacters(), false)
report.println("<h4>Digrammes de caractères (Logarithmique)</h4>")
tools.charDigrams(report, corpusStat.getReadDigrams(), corpusStat.getTotalReadCharacters(), true)

report.println("</body>")
report.println("</html>")
