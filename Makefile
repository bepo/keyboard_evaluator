LayoutComparator: clean
	(cd src && javac -encoding UTF-8 -target 1.5 org/clavierdvorak/layoutcomparator/Main.java)
	(cd src && find . -name '*.class' | xargs jar cmf ../LayoutComparator.manifest ../LayoutComparator.jar)

clean:
	find src -name '*.class' | xargs rm -f
