/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak;

import org.clavierdvorak.Key;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author twilight
 */
public class KeyTest {

    @Test
    public void shouldHandleSomeSimpleCases() {
        String k;
        k = "00";
        assertEquals(k, new Key(k).getScancodeString());
        k = "01";
        assertEquals(k, new Key(k).getScancodeString());
        k = "0F";
        assertEquals(k, new Key(k).getScancodeString());
        k = "10";
        assertEquals(k, new Key(k).getScancodeString());
        k = "1F";
        assertEquals(k, new Key(k).getScancodeString());
        k = "7F";
        assertEquals(k, new Key(k).getScancodeString());
        k = "7A";
        assertEquals(k, new Key(k).getScancodeString());
    }
    
    @Test
    public void shouldHandleAll2DigitCases() {
        String[] first16Digits = firstDigits(16);
        String[] first8Digits = firstDigits(8);
        for (String first : first8Digits) {
            for (String second : first16Digits) {
                String k = first+second;
                assertEquals(k, new Key(k).getScancodeString());
            }
        }
                
    }

    @Test
    public void shouldHandleSomeSimple4DigitCases() {
        String k;
        k = "E000";
        assertEquals(k, new Key(k).getScancodeString());
        k = "E001";
        assertEquals(k, new Key(k).getScancodeString());
        k = "E00F";
        assertEquals(k, new Key(k).getScancodeString());
        k = "E010";
        assertEquals(k, new Key(k).getScancodeString());
        k = "E01F";
        assertEquals(k, new Key(k).getScancodeString());
        k = "E07F";
        assertEquals(k, new Key(k).getScancodeString());
        k = "E07A";
        assertEquals(k, new Key(k).getScancodeString());
    }
    
    @Test
    public void shouldHandleAll4DigitCases() {
        String[] first16Digits = firstDigits(16);
        String[] first8Digits = firstDigits(8);
        for (String first : first8Digits) {
            for (String second : first16Digits) {
                String k = "E0"+first+second;
                assertEquals(k, new Key(k).getScancodeString());
            }
        }
                
    }
        
    private String[] firstDigits(int i) {
        String[] res = new String[i];
        while (--i>=0) {
            res[i] = Integer.toString(i, i+1).toUpperCase();
        }
        return res;
    }

}