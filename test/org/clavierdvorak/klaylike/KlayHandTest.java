/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.klaylike;

import org.clavierdvorak.klaylike.KlayHand;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author twilight
 */
public class KlayHandTest {

    @Test
    public void shouldHaveCorrectDimensionsForMultidimentionalArray() {
        assertEquals(5, new KlayHand().fingerPosition.length);
        assertEquals(2, new KlayHand().fingerPosition[4].length);
    }
    
    @Test
    public void shouldHaveProperInitializationsToZero() {
        KlayHand h = new KlayHand();
        for (int i = 0; i < h.fingerTotalEnergies.length; i++) {
            assertEquals("index: "+i, 0.0f, h.fingerTotalEnergies[i]);
        }
        for (int i = 0; i < h.palmPosition.length; i++) {
            assertEquals("index: "+i, 0.0f, h.palmPosition[i]);
        }
        for (int i = 0; i < h.fingerPosition.length; i++) {
            for (int j = 0; j < h.fingerPosition[i].length; j++) {
                assertEquals("indices: " + i + ", " + j, 0.0f, h.fingerPosition[i][j]);
            }
        }
    }

}
