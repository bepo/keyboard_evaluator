/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.clavierdvorak.keyselector;

import org.clavierdvorak.*;
import org.clavierdvorak.keyselector.ClassicKeyboardKeySelector;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author twilight
 */
public class ClassicKeyboardKeySelectorTest {

    static String oneLineLayout = // copied from layout_63.xkb in klay source folder
            "    key <AC01> { [                   a,            A,             ae,                  AE ] }; \n" +
            "    key <AC02> { [                   u,            U,         ugrave,              Ugrave ] }; \n" +
            "    key <AC03> { [                   i,            I, dead_diaeresis,       dead_abovedot ] }; \n" +
            "    key <AC04> { [                   e,            E,       EuroSign                      ] }; \n" +
            "    key <AC05> { [               comma,     question,   questiondown                      ] }; \n" +
            "    key <AC06> { [                   c,            C,      copyright,                cent ] }; \n" +
            "    key <AC07> { [                   t,            T,          thorn,               THORN ] }; \n" +
            "    key <AC08> { [                   s,            S,         ssharp                      ] }; \n" +
            "    key <AC09> { [                   n,            N,         ntilde,              Ntilde ] }; \n" +
            "    key <AC10> { [                   r,            R,     registered                      ] }; \n" +
            "    key <AC11> { [                   m,            M,      masculine                      ] }; \n" +
            "    key <SPCE> { [               space, nobreakspace,          space,               U202F ] }; \n";

    @Test
    public void shouldProperlyConvertSimpleSentenceWithOnlyLetters() throws IOException {
        String sentence = // uses only mapped characters, without shift, without whitespace, ...
                "itisartrarenmint";
        ClassicKeyboardKeySelector selector = new ClassicKeyboardKeySelector(new ByteArrayInputStream(oneLineLayout.getBytes()));
        Iterator<Key> it = selector.getKeySequenceFor(new StringReader(sentence));
        assertEquals("20", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());
        assertEquals("20", it.next().getScancodeString());
        assertEquals("25", it.next().getScancodeString());
        assertEquals("1E", it.next().getScancodeString());
        assertEquals("27", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());
        assertEquals("27", it.next().getScancodeString());
        assertEquals("1E", it.next().getScancodeString());
        assertEquals("27", it.next().getScancodeString());
        assertEquals("21", it.next().getScancodeString());
        assertEquals("26", it.next().getScancodeString());
        assertEquals("28", it.next().getScancodeString());
        assertEquals("20", it.next().getScancodeString());
        assertEquals("26", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());
    }

    @Test
    public void shouldProperlyConvertSimpleSentence() throws IOException {
        shouldProperlyConvertSimpleSentenceWithOnlyLetters();
        String sentence = // uses only mapped characters, without shift
                "it is art,, rare n mint";
        ClassicKeyboardKeySelector selector = new ClassicKeyboardKeySelector(new ByteArrayInputStream(oneLineLayout.getBytes()));
        Iterator<Key> it = selector.getKeySequenceFor(new StringReader(sentence));
        assertEquals("20", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("20", it.next().getScancodeString());
        assertEquals("25", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("1E", it.next().getScancodeString());
        assertEquals("27", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());

        assertEquals("22", it.next().getScancodeString());//,
        assertEquals("22", it.next().getScancodeString());//,

        assertEquals("39", it.next().getScancodeString());

        assertEquals("27", it.next().getScancodeString());
        assertEquals("1E", it.next().getScancodeString());
        assertEquals("27", it.next().getScancodeString());
        assertEquals("21", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("26", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("28", it.next().getScancodeString());
        assertEquals("20", it.next().getScancodeString());
        assertEquals("26", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());
    }

    @Test
    public void shouldProperlyConvertSentenceWithShift() throws IOException {
        shouldProperlyConvertSimpleSentenceWithOnlyLetters();
        String sentence = // uses only mapped characters, without shift
                "Is it Art?";
        ClassicKeyboardKeySelector selector = new ClassicKeyboardKeySelector(new ByteArrayInputStream(oneLineLayout.getBytes()));
        Iterator<Key> it = selector.getKeySequenceFor(new StringReader(sentence));
        assertEquals("36", it.next().getScancodeString());//right shift
        assertEquals("20", it.next().getScancodeString());
        assertEquals("25", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("20", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("36", it.next().getScancodeString());//right shift
        assertEquals("1E", it.next().getScancodeString());
        assertEquals("27", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());
                
        assertEquals("36", it.next().getScancodeString());//rigth shift
        assertEquals("22", it.next().getScancodeString());

    }
    
    @Test
    public void shouldProperlyConvertSentenceWithAltGr() throws IOException {
        shouldProperlyConvertSimpleSentenceWithOnlyLetters();
        String sentence = // uses only mapped characters, without shift
                "It is €€ æÆ";
        ClassicKeyboardKeySelector selector = new ClassicKeyboardKeySelector(new ByteArrayInputStream(oneLineLayout.getBytes()));
        Iterator<Key> it = selector.getKeySequenceFor(new StringReader(sentence));
        assertEquals("36", it.next().getScancodeString());//right shift
        assertEquals("20", it.next().getScancodeString());
        assertEquals("24", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("20", it.next().getScancodeString());
        assertEquals("25", it.next().getScancodeString());

        assertEquals("39", it.next().getScancodeString());

        assertEquals("E038", it.next().getScancodeString());//alt gr
        assertEquals("21", it.next().getScancodeString());
        assertEquals("E038", it.next().getScancodeString());//alt gr
        assertEquals("21", it.next().getScancodeString());

        assertEquals("2A", it.next().getScancodeString());//left shift
        assertEquals("39", it.next().getScancodeString());//space (for nbsp)

        assertEquals("E038", it.next().getScancodeString());//alt gr
        assertEquals("1E", it.next().getScancodeString());
        assertEquals("E038", it.next().getScancodeString());//alt gr
        assertEquals("36", it.next().getScancodeString());//right shift
        assertEquals("1E", it.next().getScancodeString());

    }}